/* eslint-disable no-undef */

/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  daisyui: {
    themes: ["light"],
  },
  theme: {
    extend: {
      colors: {
        gray: "#F8F4F3",
        gray2: "rgba(0,0,0,0.73)",
        gray3: "#F0F0F0",
        golden: "#E74100",
        chocolate: "#5D3B31",
        chocolate2: "#D2A293",
        blue: "#081C35",
        redTurk: "#B11917",
      },
      screens: {
        smartphone: "481px",
        tablet: "577px",
        laptop: "769px",
        desktop: "1025px",
        "large-width": "1441px",
        "extra-width": "1921px",
      },
      maxWidth: {
        page: "1536px",
        "page-extra": "2500px",
      },
      backgroundImage: {
        radial: "radial-gradient( rgb(94, 176, 239, 0.5), #000)",
        linear:
          "linear-gradient(rgb(34, 211, 238),rgb(34, 211, 238,0.9),rgb(34, 211, 238))",
        linearFooter: "linear-gradient(#06B6D4,#1E40AF)",
      },
    },
  },
  plugins: [require("daisyui")],
};
