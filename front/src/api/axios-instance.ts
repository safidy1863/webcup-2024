/* eslint-disable @typescript-eslint/no-explicit-any */
import { API_URL, KEY_ACCESS_TOKEN } from "@/constants";
import { showToast } from "@/helpers";
import { ETOAST } from "@/types";
import axios, { AxiosError } from "axios";

const axiosInstance = axios.create({
  baseURL: API_URL!,
  headers: {
    "Content-Type": "application/json",
  },
});

const token = localStorage.getItem(KEY_ACCESS_TOKEN);
axiosInstance.defaults.headers.common.Authorization = token
  ? `Bearer ${token}`
  : "";

axiosInstance.interceptors.response.use(
  (response) => response,
  (error: AxiosError) => {
    if (error.response) {
      return Promise.reject(error);
    }
    if ((error as any)?.code !== "ERR_CANCELED") {
      showToast(
        "Erreur de connexion. Veuillez réessayer plus tard",
        ETOAST.ERROR,
        "Erreur",
        "text-[#FF4B4B]"
      );
    }
  }
);

export { axiosInstance };
