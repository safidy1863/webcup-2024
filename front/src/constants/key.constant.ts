const KEY_USER_STORAGE = "user";
const KEY_ACCESS_TOKEN = "access-token";

export { KEY_USER_STORAGE, KEY_ACCESS_TOKEN };
