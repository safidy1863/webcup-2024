import logo from "./logo/logo.svg";

import banner from "./images/banner.webp";
import bannerDetails from "./images/banner-details.png";
import bannerBasket from "./images/banner-basket.png";
import bannerEvents from "./images/banner-events.png";
import hamburger from "./images/hamburger.svg";
import flour from "./images/flour.png";
import flour2 from "./images/flour-2.png";
import textureChocolate from "./images/texture-chocolate.png";
import textureWhite from "./images/texture-white.png";
import textureGray from "./images/texture-gray.png";
import textureGray2 from "./images/texture-gray-2.png";
import textureGray3 from "./images/texture-gray-3.png";
import textureEventTop from "./images/texture-event-top.png";
import textureEventBottom from "./images/texture-event-bottom.png";
import tree from "./images/tree.png";
import bgHour from "./images/bg-hour.png";
import sheeiff from "./images/sheeiff.png";
import cowboy from "./images/cowboy.jpeg";
import bas from "./images/bas.png";
import shapeFooter from "./images/shape-footer.png";
import bgEvent from "./images/bg-event.png";
import blurBurger from "./images/blur-burger.png";

import food1 from "./temp/food-1.png";
import food2 from "./temp/food-2.png";
import food3 from "./temp/food-5.webp";
import table from "./temp/table.webp";
import chair1 from "./temp/chair.webp";
import chair2 from "./temp/chair-2.webp";
import chair3 from "./temp/char-3.webp";
import user1 from "./temp/user.png";
import user2 from "./temp/user-2.png";
import user3 from "./temp/user-3.png";
import event1 from "./temp/event-1.png";

import food5 from "./temp/food-1.png";
import food6 from "./temp/food-2.png";
import food7 from "./temp/food-3.png";
import food8 from "./temp/food-4.png";

export {
  logo,
  banner,
  bannerDetails,
  bannerBasket,
  bannerEvents,
  blurBurger,
  hamburger,
  flour,
  textureChocolate,
  textureWhite,
  textureGray,
  textureGray2,
  textureGray3,
  textureEventTop,
  textureEventBottom,
  bgEvent,
  tree,
  bgHour,
  sheeiff,
  cowboy,
  bas,
  shapeFooter,
  food1,
  food2,
  food3,
  table,
  chair1,
  chair2,
  chair3,
  flour2,
  user1,
  user2,
  user3,
  event1,
  food5,
  food6,
  food7,
  food8,
};
