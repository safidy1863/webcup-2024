import { ToastContext } from "@/contexts";
import { useContext } from "react";

export const useToast = () => {
  const context = useContext(ToastContext);
  if (!context) {
    throw new Error("useToast est obligatoirement dans un ToastProvider");
  }
  return context;
};
