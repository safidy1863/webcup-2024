import { useEffect, useState } from "react";

type TUseModalProps = {
  modalId: string;
};

export const useModal = (props: TUseModalProps) => {
  const { modalId } = props;
  const [modal, setModal] = useState<HTMLDialogElement | null>(null);

  const openModal = () => {
    if (modal) {
      modal.showModal();
    }
  };

  const closeModal = () => {
    if (modal) {
      modal.close();
    }
  };

  useEffect(() => {
    setModal(document.getElementById(modalId) as HTMLDialogElement);
  }, [modalId]);

  return {
    openModal,
    closeModal,
  };
};
