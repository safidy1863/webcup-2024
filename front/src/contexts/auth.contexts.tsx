/* eslint-disable react-hooks/exhaustive-deps */
import { TUser } from "@/types";
import { ReactNode, createContext, useEffect, useMemo, useState } from "react";

type TConnectedUser = Partial<TUser>;

interface IAuthContextValue {
  user: TConnectedUser | null;
  login: (user: TConnectedUser) => void;
  logout: VoidFunction;
  setAccess: VoidFunction;
  getAccess: () => [boolean];
}

type TAuthProviderProps = {
  children: ReactNode;
};

export const AuthContext = createContext<IAuthContextValue | undefined>(
  undefined
);

export const AuthProvider = (props: TAuthProviderProps) => {
  const { children } = props;
  const [user, setUser] = useState<TConnectedUser | null>(null);

  useEffect(() => {
    const storedUser = localStorage.getItem("user");
    if (storedUser) {
      setUser(JSON.parse(storedUser));
    }
  }, []);

  const login = (userData: TConnectedUser) => {
    localStorage.setItem("user", JSON.stringify(userData));
    setUser(userData);
  };

  const logout = () => {
    localStorage.clear();
    setUser(null);
  };

  const getAccess = () => {
    const accesses = localStorage.getItem("ACCESS");
    return JSON.parse(accesses || "false");
  };

  const setAccess = () => {
    localStorage.setItem("ACCESS", JSON.stringify([true]));
  };

  const contextValue = useMemo(
    () => ({ user, login, logout, getAccess, setAccess }),
    [user, login, logout]
  );

  return (
    <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
  );
};
