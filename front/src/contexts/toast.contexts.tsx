/* eslint-disable react-hooks/exhaustive-deps */
import { ETOAST } from "@/types";
import { Icon } from "@iconify/react";
import { ReactNode, createContext, useMemo } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const headerClass = {
  [ETOAST.ERROR]: "text-[#FF4B4B]",
  [ETOAST.INFO]: "text-[#3387EA]",
  [ETOAST.SUCCESS]: "text-[#1FA591]",
  [ETOAST.WARNING]: "text-[#F5AF03]",
};

const icons = {
  [ETOAST.ERROR]: "fluent-mdl2:status-error-full",
  [ETOAST.INFO]: "ic:round-info",
  [ETOAST.SUCCESS]: "icon-park-solid:check-one",
  [ETOAST.WARNING]: "fluent:warning-28-filled",
};

interface IToastContextProps {
  showToast: (message: string, severity: ETOAST, title: string) => void;
}

type TToastProviderProps = {
  children: ReactNode;
};

export const ToastContext = createContext<IToastContextProps | undefined>(
  undefined
);

export const ToastProvider = (props: TToastProviderProps) => {
  const { children } = props;
  const showToast = (message: string, severity: ETOAST, title: string) => {
    toast(
      <div>
        <h3 className={`${headerClass[severity]} `}>{title}</h3>
        <p>{message}</p>
      </div>,
      {
        type: severity,
        className: "bg-white",
        icon: (
          <Icon
            icon={icons[severity]}
            className={`${headerClass[severity]} text-2xl`}
          />
        ),
      }
    );
  };

  const contextValue = useMemo(() => ({ showToast }), [showToast]);

  return (
    <ToastContext.Provider value={contextValue}>
      <ToastContainer
        toastClassName={() =>
          "flex p-0 min-h-10 rounded-md justify-between overflow-hidden cursor-pointer"
        }
        bodyClassName={() =>
          "text-sm font-white font-med flex p-3 items-start border-left"
        }
        position="top-right"
        autoClose={3000}
        hideProgressBar
      />
      {children}
    </ToastContext.Provider>
  );
};
