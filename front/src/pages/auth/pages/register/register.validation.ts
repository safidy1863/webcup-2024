import { TUserDto } from "@/types";
import * as yup from "yup";

export const registerValidation: yup.ObjectSchema<TUserDto> = yup.object({
  pseudo: yup.string().required("Votre pseudo est obligatoire"),
  email: yup
    .string()
    .email("Veuillez entrer un adresse email valide")
    .required("Votre adresse email est obligatoire"),
  password: yup
    .string()
    .required("Votre mot de passe est obligatoire")
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
      "Le mot de passe doit contenir au moins 8 caractères, une lettre, un chiffre et un caractère spécial"
    ),
  confirmPassword: yup.string().when("password", {
    is: (password: string) => password && password.length > 0,
    then: () =>
      yup
        .string()
        .required("La confirmation du mot de passe est requise")
        .oneOf([yup.ref("password")], "Les mots de passe doivent correspondre"),
  }),
});
