import { Button, ControlledInput, Form } from "@/components";
import { showBackdropLoader } from "@/helpers";
import { useModal, useValidationResolver } from "@/hooks";
import { idModalAuth } from "@/pages";
import { useRegister } from "@/pages/auth/hooks/register";
import { registerValidation } from "@/pages/auth/pages/register/register.validation";
import { EButtonForm, TUserDto } from "@/types";
import { Icon } from "@iconify/react";
import { useForm } from "react-hook-form";

export const Register = () => {
  const { closeModal } = useModal({ modalId: idModalAuth.register });
  const form = useForm<TUserDto>({
    mode: "onSubmit",
    resolver: useValidationResolver(registerValidation),
  });

  const { control, handleSubmit } = form;

  const afterRegister = () => {
    showBackdropLoader(false);
    closeModal();
  };
  const { mutateAsync } = useRegister({ action: afterRegister });

  const register = () => {
    handleSubmit(async (values) => {
      showBackdropLoader(true, "Création de nouveau utilisateur en cours...");
      await mutateAsync({ data: values });
    })();
  };

  return (
    <div className="p-5 flex flex-col gap-y-5">
      <div className="border border-white border-opacity-10 rounded-full h-16 w-16 flex justify-center items-center ml-[50%] -translate-x-1/2">
        <Icon icon="mdi:email-plus-outline" className="text-4xl" />
      </div>

      <h3 className="text-white text-2xl font-semibold text-center">
        Request Access
      </h3>
      <p className="text-center">
        Join our growing waitlist of 667 people and our team
        <br />
        will reach out to you as soon as possible.
      </p>

      <div className="flex flex-col gap-y-5">
        <Form {...form}>
          <ControlledInput
            control={control}
            name="pseudo"
            label="Name"
            placeholder="Enter your name"
          />
          <ControlledInput
            control={control}
            name="email"
            label="Email"
            placeholder="Your email address"
            type="email"
          />
          <ControlledInput
            control={control}
            name="password"
            label="Password"
            placeholder="Enter your password"
            type="password"
          />
          <ControlledInput
            control={control}
            name="confirmPassword"
            label="Confirmation of password"
            placeholder="Enter your confirmation of password"
            type="password"
          />
          <Button
            text="Request access"
            type={EButtonForm.SQUARE}
            className="w-full bg-gray4 text-gray6"
            onClick={register}
          />
        </Form>
      </div>
    </div>
  );
};
