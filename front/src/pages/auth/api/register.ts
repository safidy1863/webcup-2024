import { axiosInstance } from "@/api";
import { urlsAuth } from "@/pages/auth/api/url";
import { TUserDto } from "@/types";

export const register = async ({ data }: { data: TUserDto }) => {
  const response = await axiosInstance.post(`${urlsAuth.auth}/register/`, data);
  return response;
};
