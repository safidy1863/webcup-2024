/* eslint-disable @typescript-eslint/no-explicit-any */
import { showBackdropLoader } from "@/helpers";
import { useToast } from "@/hooks";
import { register } from "@/pages/auth/api";
import { ETOAST, TUserDto } from "@/types";
import { EHttpStatus } from "@/types/app/https-requests";
import { useMutation } from "@tanstack/react-query";
import { AxiosError } from "axios";

type TUseRegister = {
  action: VoidFunction;
};

export const useRegister = (props: TUseRegister) => {
  const { action } = props;
  const { showToast } = useToast();

  const mutation = useMutation({
    mutationFn: (mutateData: { data: TUserDto }) => register(mutateData),
    onSuccess: (response) => {
      if (response.status === EHttpStatus.CREATED) {
        showToast(
          "Création de nouveau utilisateur réussi...",
          ETOAST.SUCCESS,
          "Success"
        );
        action();
      } else {
        showToast(
          "Erreur durant la création de l'utilisateur",
          ETOAST.ERROR,
          "Erreur"
        );
      }
      showBackdropLoader(false);
    },
    onError: (error: AxiosError) => {
      console.log(error);
      showToast(
        "Erreur durant la création de l'utilisateur",
        ETOAST.ERROR,
        "Erreur"
      );
      showBackdropLoader(false);
    },
  });

  return mutation;
};
