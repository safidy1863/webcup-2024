import { pathsAdmin } from "@/pages/admin/constants";
import {
  Dashboard,
  Functionnalities,
  FunctionnalitiesRouter,
} from "@/pages/admin/pages";
import { TRoute } from "@/types";
import { Navigate } from "react-router-dom";

export const AdminRouter: TRoute[] = [
  {
    path: pathsAdmin.dashboard,
    element: <Dashboard />,
  },
  {
    path: pathsAdmin.functionnalities,
    element: <Functionnalities />,
    children: FunctionnalitiesRouter,
  },
  {
    path: "",
    element: <Navigate to={pathsAdmin.dashboard} replace />,
  },
];
