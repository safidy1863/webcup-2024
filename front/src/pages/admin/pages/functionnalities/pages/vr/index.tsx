import { VrRouter } from "@/pages/admin/pages/functionnalities/pages/vr/routes";
import { Outlet } from "react-router-dom";

const Vr = () => {
  return (
    <>
      <Outlet />
    </>
  );
};

export { Vr, VrRouter };
