import { pathsVr } from "@/pages/admin/pages/functionnalities/pages/vr/constants";
import { VrBasics } from "@/pages/admin/pages/functionnalities/pages/vr/pages";
import { TRoute } from "@/types";
import { Navigate } from "react-router-dom";

export const VrRouter: TRoute[] = [
  {
    path: pathsVr.vrBasics,
    element: <VrBasics />,
  },
  {
    path: "",
    element: <Navigate to={pathsVr.vrBasics} replace />,
  },
];
