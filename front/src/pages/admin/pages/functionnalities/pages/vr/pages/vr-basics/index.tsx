import { OrbitControls } from "@react-three/drei";
import { Canvas } from "@react-three/fiber";
// import { DoubleSide, TextureLoader } from "three";

export const VrBasics = () => {

  return (
    <div className="h-full ">
      <Canvas>
        <OrbitControls />
        <mesh>
          <sphereGeometry args={[15, 32, 16]} />
          {/* <meshBasicMaterial map={textureImage} side={DoubleSide} /> */}
        </mesh>
      </Canvas>
    </div>
  );
};
