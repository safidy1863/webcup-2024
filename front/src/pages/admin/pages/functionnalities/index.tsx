import { Outlet } from "react-router-dom";
import { FunctionnalitiesRouter } from "@/pages/admin/pages/functionnalities/routes";

const Functionnalities = () => {
  return <Outlet />;
};

export { FunctionnalitiesRouter, Functionnalities };
