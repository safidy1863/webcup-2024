import { pathsFunctionnalities } from "@/pages/admin/pages/functionnalities/constants";
import { Maps, Vr, VrRouter } from "@/pages/admin/pages/functionnalities/pages";
import { TRoute } from "@/types";
import { Navigate } from "react-router-dom";

export const FunctionnalitiesRouter: TRoute[] = [
  {
    path: pathsFunctionnalities.maps,
    element: <Maps />,
  },
  {
    path: pathsFunctionnalities.vr,
    element: <Vr />,
    children: VrRouter,
  },
  {
    path: "",
    element: <Navigate to={pathsFunctionnalities.maps} replace />,
  },
];
