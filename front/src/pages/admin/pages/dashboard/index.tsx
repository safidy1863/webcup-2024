export const Dashboard = () => {
  return (
    <main className="h-screen">
      <iframe
        src="https://dashwebcup.codeo.mg/"
        width="100%"
        height="100%"
        loading="lazy"
        referrerPolicy="no-referrer-when-downgrade"
      ></iframe>
    </main>
  );
};
