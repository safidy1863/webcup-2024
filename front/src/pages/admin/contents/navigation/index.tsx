import Avatar from "@/components/avatar";
import { Icon } from "@iconify/react";

export const Navigation = () => {
  return (
    <div className="flex justify-end">
      <div className="flex">
        {/* Searchbar */}
        <div className="flex gap-4 items-center ">
          <label className=" input h-10 input-bordered flex items-center gap-2">
            <Icon icon="iconamoon:search-thin" />
            <input
              type="text"
              className="grow"
              placeholder="Rechercher ici..."
            />
          </label>
        </div>

        <div className="dropdown  dropdown-end">
          <div tabIndex={0} role="button" className="m-1">
            <Avatar src="" name="Safidy" />
          </div>
          <ul
            tabIndex={0}
            role="button"
            className="dropdown-content z-[1] menu p-2 shadow bg-white dark:bg-grayFourdly rounded-md min-w-52"
          >
            <li>
             <button>Se deconnecter</button>
            </li>
          </ul>
        </div>

        <div className="relative">{/* <ThemeSwitcher /> */}</div>
      </div>
    </div>
  );
};
