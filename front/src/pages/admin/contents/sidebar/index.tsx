import { logo } from "@/assets";
import { NavigationItem } from "./item";
import { navigations } from "@/pages/admin/data";

export const Sidebar = () => {
  return (
    <div className="w-[300px] bg-gray7 py-5">
      <div className="px-5 pb-5 border-b border-gray2">
        <img src={logo} alt="logo" className="w-32" />
      </div>

      <div className="px-5 py-5 flex flex-col gap-y-1">
        {navigations.map((navigation, index) => <NavigationItem key={index} navigation={navigation}/>)}
      </div>
    </div>
  );
};
