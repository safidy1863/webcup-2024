import { Icon } from "@iconify/react";
import { TNavDocs } from "../../types/app/nav-docs.type";
import { NavLink } from "react-router-dom";

type TNavigationItemProps = {
  navigation: TNavDocs;
};

export const NavigationItem = (props: TNavigationItemProps) => {
  const { navigation } = props;
  const { icon, menu, subMenu, path } = navigation;

  return (
    <NavLink to={path || ""} className="text-gray11">
      {({ isActive }) => (
        <>
          <div
            className={`flex items-center gap-x-3 py-2 px-3 rounded ${
              isActive && "text-gray10 bg-gray9 font-semibold"
            }`}
          >
            <Icon icon={icon} className="text-xl" />
            <span className={`text-base ${isActive && "font-semibold"}`}>
              {menu}
            </span>
          </div>

          {subMenu?.map((sub, index) => (
            <NavLink to={`${path}/${sub.path}` || ""} key={index}>
              {({ isActive: activeSub }) => (
                <>
                  <div
                    className={`pl-5 pt-3 flex items-center gap-x-2 ${
                      activeSub && "text-gray10 font-semibold"
                    }`}
                  >
                    <div className="flex items-center gap-x-1 text-xl">
                      <Icon icon="bi:caret-right-fill" />
                      <Icon icon={sub.icon} />
                    </div>
                    <span className="text-base">{sub.menu}</span>
                  </div>
                </>
              )}
            </NavLink>
          ))}
        </>
      )}
    </NavLink>
  );
};
