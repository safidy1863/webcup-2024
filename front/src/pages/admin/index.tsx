import { AdminRouter } from "@/pages/admin/routes";
import { Outlet } from "react-router-dom";

const Admin = () => {
  return <Outlet />;
};

export { AdminRouter, Admin };
