import { pathsAdmin } from "@/pages/admin/constants";
import { TNavDocs } from "../types/app/nav-docs.type";
import { pathsFunctionnalities } from "@/pages/admin/pages/functionnalities/constants";

export const navigations: Array<TNavDocs> = [
  {
    icon: "solar:home-angle-bold",
    menu: "Home",
    path: pathsAdmin.dashboard,
  },
  {
    icon: "iconoir:post-solid",
    menu: "Fonctionnalites",
    path: pathsAdmin.functionnalities,
    subMenu: [
      {
        icon: "simple-icons:googlemaps",
        menu: "Maps",
        path: pathsFunctionnalities.maps,
      },
      {
        icon: "ph:virtual-reality-duotone",
        menu: "VR",
        path: pathsFunctionnalities.vr,
      },
    ],
  },
];
