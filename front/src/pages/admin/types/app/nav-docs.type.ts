export type TNavDocs = {
  icon: string;
  menu: string;
  path?: string;
  subMenu?: Array<TNavDocs>;
};
