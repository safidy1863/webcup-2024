import { Outlet } from "react-router-dom";
import { ClientsRouter } from "@/pages/clients/routes";
import {
  Navigation,
  NavigationDrawer,
  ReservationTable,
} from "@/pages/clients/contents";
import { Footer } from "@/pages/clients/contents/footer";
import { Dialog } from "@/components/dialog";
import { idDialog } from "@/pages/clients/constants";
import { ControlledDrawer } from "@/components";
import { clientSignal } from "@/pages/clients/client-signal";
import { useSignals } from "@preact/signals-react/runtime";
import { PersonnalisationData } from "@/pages/clients/contents/personnalisation-data";

const Clients = () => {
  useSignals();

  return (
    <div className="min-h-screen bg-gray">
      <Navigation />
      <main>
        <Outlet />
      </main>
      <Footer />
      <Dialog modalId={idDialog.reservation}>
        <ReservationTable />
      </Dialog>

      <Dialog modalId={idDialog.personnalisationData}>
        <PersonnalisationData />
      </Dialog>

      {clientSignal.value.openNavigation && (
        <ControlledDrawer
          open={clientSignal.value.openNavigation}
          title=""
          onClose={() =>
            (clientSignal.value = {
              ...clientSignal.value,
              openNavigation: false,
            })
          }
          className="!bg-chocolate"
          size="MIN"
        >
          <NavigationDrawer />
        </ControlledDrawer>
      )}
    </div>
  );
};

export { Clients, ClientsRouter };
