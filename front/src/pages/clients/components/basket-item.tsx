import { API_URL } from "@/constants";
import { TFood } from "@/pages/clients/types";

type Props = {
  food: TFood;
};

export const BasketItem = (props: Props) => {
  const { food } = props;
  const { image, name, price } = food;

  return (
    <div className="flex gap-x-1 items-center text-chocolate">
      <img
        src={`${API_URL}/${image}`}
        alt={name}
        className="w-16 h-16 object-cover rounded-lg"
      />
      <div className="flex-1">
        <p>{name}</p>
        <p>{price}</p>
      </div>
    </div>
  );
};
