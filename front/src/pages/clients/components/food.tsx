import { blurBurger } from "@/assets";
import { pathsClients } from "@/pages/clients/constants";
import { useSaveLocalStorage } from "@/pages/clients/hooks/use-save-food-localstorage";
import { TFood } from "@/pages/clients/types";
import { Icon } from "@iconify/react";
import { Link } from "react-router-dom";

type TFoodProps = {
  food: TFood;
  id: number;
};

export const Food = (props: TFoodProps) => {
  const { food, id } = props;
  const { category, name, price, description } = food;
  const { addNewFood } = useSaveLocalStorage();

  const newMarket = () => {
    addNewFood({ ...food, id: id.toString() });
  };

  return (
    <article className="relative pb-10 w-full p-4 rounded-[32px] bg-white hover:shadow-2xl transition ease-out duration-200">
      <div className="w-full relative h-52">
        <img
          src={blurBurger}
          alt={name}
          className="absolute inset-0 rounded-[28px]"
        />

        <img
          src={`/food-${id}.png`}
          alt={name}
          className="z-20 absolute -top-10 -left-10 h-52 w-full rounded-md object-cover"
        />

        <Link
          to={`/${pathsClients.basket}`}
          onClick={newMarket}
          className="absolute z-30 right-3 top-3 text-2xl bg-golden rounded-full text-white p-2"
        >
          <Icon icon="mage:basket" />
        </Link>
      </div>

      <div className="mt-14">
        <h4 className="text-chocolate mb-2 ">{name}</h4>
        <p className="line-clamp-3 mb-10 opacity-60">{description}</p>
        <div className="mt-5 absolute bottom-1 right-3 left-4 flex justify-between py-2">
          <p className=" text-2xl text-chocolate font-semibold">${price}</p>
          <p className="bg-golden bg-opacity-15 text-golden py-1 px-4 rounded-2xl">
            {category}
          </p>
        </div>
      </div>
    </article>
  );
};
