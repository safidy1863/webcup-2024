import { useState } from "react";

type TNumberProps = {
  currentValue: number;
  getNumber: (state: number, isPlus?: boolean) => void;
};

export const Number = (props: TNumberProps) => {
  const { getNumber, currentValue } = props;
  const [currentNumber, setCurrentNumber] = useState<number>(currentValue);

  const plus = () => {
    getNumber(currentNumber + 1, true);
    setCurrentNumber((current) => current + 1);
  };

  const minus = () => {
    if (currentNumber > 1) {
      getNumber(currentNumber - 1);
      setCurrentNumber((current) => current - 1);
    }
  };

  return (
    <div className="bg-gray3 flex justify-between items-center rounded-md p-3">
      <button onClick={plus} className="text-5xl h-14 w-14 bg-white rounded-md">
        +
      </button>
      <span>{currentNumber}</span>
      <button
        onClick={minus}
        className="text-5xl h-14 w-14 bg-white rounded-md"
      >
        -
      </button>
    </div>
  );
};
