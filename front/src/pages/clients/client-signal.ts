import { signal } from "@preact/signals-react";

export const clientSignal = signal<{ openNavigation: boolean }>({
  openNavigation: false,
});
