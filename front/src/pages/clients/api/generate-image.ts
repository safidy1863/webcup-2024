import axios from "axios";

export const generateImage = async (prompt: string) => {
  const response = await axios.post("https://chat.codeo.mg/generate-image", {
    prompt,
  });
  return response;
};
