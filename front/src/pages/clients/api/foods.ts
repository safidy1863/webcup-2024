import { axiosInstance } from "@/api";
import { urlsClient } from "@/pages/clients/api";
import { TEvent, TFood } from "@/pages/clients/types";

export const getFoods = async () => {
  const response = await axiosInstance.get<TFood[]>(`${urlsClient.food}`);
  return response.data;
};

export const payFood = async (price: number) => {
  const response = await axiosInstance.post(`${urlsClient.food}/pay`, {
    price,
  });
  return response;
};

export const reservation = async (data: {
  name: string;
  email: string;
  phone: string;
  categoryId: number;
  date: string;
  message: string;
}) => {
  const response = await axiosInstance.post(`${urlsClient.table}/book`, data);
  return response;
};

export const getEvents = async () => {
  const response = await axiosInstance.get<TEvent[]>(`${urlsClient.event}`);
  return response.data;
};