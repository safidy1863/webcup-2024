/* eslint-disable @typescript-eslint/no-explicit-any */
import { showBackdropLoader } from "@/helpers";
import { useToast } from "@/hooks";
import { generateImage } from "@/pages/clients/api/generate-image";
import { ETOAST } from "@/types";
import { EHttpStatus } from "@/types/app/https-requests";
import { useMutation } from "@tanstack/react-query";
import { AxiosError } from "axios";


export const useGenerateImage = () => {
  const { showToast } = useToast();

  const mutation = useMutation({
    mutationFn: (mutateData: string) => generateImage(mutateData),
    onSuccess: (response) => {
      if (response.status === EHttpStatus.CREATED) {
        showToast(
          "Generate image...",
          ETOAST.SUCCESS,
          "Success"
        );
        // action(response.data.url);
        console.log(response)
      } else {
        showToast(
          "Erreur durant le paiement du menu...",
          ETOAST.ERROR,
          "Erreur"
        );
      }
      showBackdropLoader(false);
    },
    onError: (error: AxiosError) => {
      console.log(error);
      showToast("Erreur durant le paiement du menu..", ETOAST.ERROR, "Erreur");
      showBackdropLoader(false);
    },
  });

  return mutation;
};
