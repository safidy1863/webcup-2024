import { TFood } from "@/pages/clients/types";
import { useEffect, useState } from "react";

export const useSaveLocalStorage = () => {
  const [foods, setFoods] = useState<TFood[]>([]);

  const addNewFood = (food: TFood) => {
    const valueLocalstorage = localStorage.getItem("payement-food");

    if (valueLocalstorage !== null) {
      const last = JSON.parse(valueLocalstorage) as TFood[];
      last.push(food);
      localStorage.setItem("payement-food", JSON.stringify(last));
    } else {
      const values = [];
      values.push(food);
      localStorage.setItem("payement-food", JSON.stringify(values));
    }

    setFoods((currentValue) => [...currentValue, food]);
  };

  const getAllFoods = () => {
    const valueLocalstorage = localStorage.getItem("payement-food");
    console.log(valueLocalstorage);
    setFoods(
      valueLocalstorage ? (JSON.parse(valueLocalstorage) as TFood[]) : []
    );
  };

  const resetBasket = () => {
    localStorage.removeItem("payement-food");
  };

  useEffect(() => {
    getAllFoods();
  }, []);

  return {
    foods,
    addNewFood,
    resetBasket,
  };
};
