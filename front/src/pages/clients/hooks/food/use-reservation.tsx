/* eslint-disable @typescript-eslint/no-explicit-any */
import { showBackdropLoader } from "@/helpers";
import { useToast } from "@/hooks";
import { reservation } from "@/pages/clients/api";
import { ETOAST } from "@/types";
import { EHttpStatus } from "@/types/app/https-requests";
import { useMutation } from "@tanstack/react-query";
import { AxiosError } from "axios";

type TUsePay = {
  action: () => void;
};

export const useReservation = (props: TUsePay) => {
  const { action } = props;
  const { showToast } = useToast();

  const mutation = useMutation({
    mutationFn: (mutateData: {
      name: string;
      email: string;
      phone: string;
      categoryId: number;
      date: string;
      message: string;
    }) => reservation(mutateData),
    onSuccess: (response) => {
      if (response.status === EHttpStatus.CREATED) {
        showToast("Réservation réussi...", ETOAST.SUCCESS, "Success");
        action();
      } else {
        showToast("Erreur durant la réservation...", ETOAST.ERROR, "Erreur");
      }
      showBackdropLoader(false);
    },
    onError: (error: AxiosError) => {
      console.log(error);
      showToast("Erreur durant la réservation...", ETOAST.ERROR, "Erreur");
      showBackdropLoader(false);
    },
  });

  return mutation;
};
