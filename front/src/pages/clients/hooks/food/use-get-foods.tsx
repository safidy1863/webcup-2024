/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { useToast } from "@/hooks";
import { QueryCacheKey, getFoods } from "@/pages/clients/api";
import { ETOAST } from "@/types";
import { useQuery } from "@tanstack/react-query";
import { useEffect } from "react";


export const useGetFoods = () => {
  const { showToast } = useToast();

	const { isLoading, data, refetch, error } = useQuery(
		[QueryCacheKey.GET_LIST],
		() => getFoods(),
	);

	const loading = isLoading;

	useEffect(() => {
		if (error) {
			if ((error as any)?.code !== 'ERR_CANCELED') {
				showToast(
					'Erreur lors de la recuperation de la liste des menus',
					ETOAST.ERROR,
					'Erreur',
				);
			}
		}
	}, [error]);

	return {
		refetch,
		loading,
		data,
		error,
	};
};
