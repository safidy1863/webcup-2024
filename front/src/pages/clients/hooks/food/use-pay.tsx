/* eslint-disable @typescript-eslint/no-explicit-any */
import { showBackdropLoader } from "@/helpers";
import { useToast } from "@/hooks";
import { payFood } from "@/pages/clients/api";
import { ETOAST } from "@/types";
import { EHttpStatus } from "@/types/app/https-requests";
import { useMutation } from "@tanstack/react-query";
import { AxiosError } from "axios";

type TUsePay = {
  action: (url: string) => void;
};

export const usePay = (props: TUsePay) => {
  const { action } = props;
  const { showToast } = useToast();

  const mutation = useMutation({
    mutationFn: (mutateData: number) => payFood(mutateData),
    onSuccess: (response) => {
      if (response.status === EHttpStatus.CREATED) {
        showToast(
          "Redirection dans l'interface de paiement...",
          ETOAST.SUCCESS,
          "Success"
        );
        action(response.data.url);
      } else {
        showToast(
          "Erreur durant le paiement du menu...",
          ETOAST.ERROR,
          "Erreur"
        );
      }
      showBackdropLoader(false);
    },
    onError: (error: AxiosError) => {
      console.log(error);
      showToast("Erreur durant le paiement du menu..", ETOAST.ERROR, "Erreur");
      showBackdropLoader(false);
    },
  });

  return mutation;
};
