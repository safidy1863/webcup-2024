import { bannerDetails } from "@/assets"
import { motion } from "framer-motion"

export const Header = () => {
  return (
    <header className="relative h-80 ">
      <img
        src={bannerDetails}
        alt="banner-details"
        className="absolute inset-0 h-full w-full object-cover"
      />
      <motion.div
      variants={{
        hidden: { opacity: 0, y: 75},
        visible: { opacity: 1, y:0 },
      }}
      initial="hidden"
      animate="visible"
      transition={{ duration: 1, delay: 0.25 }}
      className="absolute overflow-y-hidden inset-0 z-30 flex flex-col items-center justify-center padding">
        <div className="text-white flex flex-col gap-x-3 justify-center items-center">
          <div className="w-8 h-1 bg-golden -translate-y-1/2"></div>
          <h3>MENU</h3>
        </div>

        <h2 className="font-rougth text-6xl text-white text-center">
          EXPLOREZ NOTRE MENU DE <br className="hidden laptop:block"/> L'OUEST AMERICAIN
        </h2>
      </motion.div>
    </header>
  );
};
