import { Food } from "@/pages/clients/components";
import { TFood } from "@/pages/clients/types";

type TPropostionsProps = {
  data: TFood[];
};

export const Propositions = (props: TPropostionsProps) => {
  const { data } = props;

  return (
    <section className="mt-5 max-page padding">
      <div className="flex flex-col gap-x-3 items-center justify-start">
        <div className="w-8 h-1 bg-golden -translate-y-1/2"></div>
        <h3>NOTRE MENU</h3>
      </div>
      <h2 className="font-rougth text-center text-6xl text-chocolate">
        MENU DES PROPOSITIONS SPECIALES
      </h2>

      <div className="grid grid-cols-2 desktop:flex mt-5 justify-between gap-5">
        {data
          ?.filter(
            (item) =>
              item.category.toLowerCase() === "Personnalisé".toLowerCase()
          )
          .map((food, index) => (
            <Food key={food.id} id={index + 1} food={food} />
          ))}
      </div>
    </section>
  );
};
