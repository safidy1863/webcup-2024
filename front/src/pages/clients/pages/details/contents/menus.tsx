import { Menu } from "@/pages/clients/pages/details/components";
import { TFood } from "@/pages/clients/types";
import { useState } from "react";

const filters: string[] = ["TOUT", "BURGER", "TEX-MEX", "STEAK"];

type TMenusProps = {
  data: TFood[];
};

export const Menus = (props: TMenusProps) => {
  const { data } = props;
  const [filterValue, setFilterValue] = useState<string>("TOUT");

  const handleClickFilter = (value: string) => {
    setFilterValue(value);
  };

  return (
    <section className="pt-5  bg-chocolate bg-opacity-[8%]">
      <div className="padding max-page">
        <div className="flex justify-center items-center gap-x-5 text-chocolate">
          {filters.map((filter, index) => (
            <span
              key={index}
              className={`cursor-pointer ${
                filterValue === filter &&
                "text-white bg-chocolate py-1 px-8 rounded-full"
              }`}
              onClick={() => handleClickFilter(filter)}
            >
              {filter}
            </span>
          ))}
        </div>

        <div className="mt-10">
          <div className="flex flex-col gap-x-3 items-center justify-start">
            <div className="w-8 h-1 bg-golden -translate-y-1/2"></div>
            <h3 className="py-2">NOTRE MENU</h3>
          </div>
          <h2 className="font-rougth text-center text-6xl text-chocolate">
            {filterValue}
          </h2>
        </div>

        <div className="grid laptop:grid-cols-2 max-w-[1000px] mx-auto gap-3 gap-y-3 mt-5">
          {data
            ?.filter((item) => item.category !== "American")
            .filter((item) => {
              if (filterValue === "TOUT") {
                return item;
              } else {
                return (
                  item.category.toLowerCase() === filterValue.toLowerCase()
                );
              }
            })
            .map((food, index) => (
              <Menu key={index} food={food} />
            ))}
        </div>
      </div>
    </section>
  );
};
