import { API_URL } from "@/constants";
import { useModal } from "@/hooks";
import { idDialog, pathsClients } from "@/pages/clients/constants";
import { TFood } from "@/pages/clients/types";
import { Icon } from "@iconify/react";
import { Link } from "react-router-dom";

type TMenuProps = {
  food: TFood;
};

export const Menu = (props: TMenuProps) => {
  const { food } = props;
  const { category, image, name, price, description } = food;
  const { openModal } = useModal({ modalId: idDialog.personnalisationData });

  return (
    <article
      onClick={openModal}
      className="w-full cursor-pointer p-4 flex  gap-x-5 rounded-[32px] bg-white hover:shadow-2xl transition ease-out duration-200"
    >
      <div className="relative h-32 w-32 rounded-[32px]">
        <img
          src={`${API_URL}/${image}`}
          alt={name}
          className="h-full w-full object-cover rounded-[28px]"
        />
      </div>

      <div className="mt-3 relative flex-1">
        <div className="flex">
          <div className="flex-1">
            <h4 className="text-chocolate font-bold">{name}</h4>
            <p className="line-clamp-2 mt-1 opacity-60">{description}</p>
          </div>
          <p className="text-lg text-chocolate font-semibold">${price}</p>
        </div>

        <div className="absolute left-0 right-0 bottom-0 flex justify-between">
          <p className="bg-golden bg-opacity-15 text-golden p-2 px-4 rounded-2xl">
            {category}
          </p>
          <Link
            to={`/${pathsClients.basket}`}
            className="text-2xl bg-golden rounded-full text-white p-2"
          >
            <Icon icon="mage:basket" />
          </Link>
        </div>
      </div>
    </article>
  );
};
