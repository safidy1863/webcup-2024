import { Hours } from "@/pages/clients/contents";
import { useGetFoods } from "@/pages/clients/hooks/food";
import {
  Header,
  Menus,
  Propositions,
} from "@/pages/clients/pages/details/contents";

export const Details = () => {
  const { data } = useGetFoods();

  return (
    <>
      <Header />
      <Menus data={data || []} />
      <Hours />
      <Propositions data={data || []} />
    </>
  );
};
