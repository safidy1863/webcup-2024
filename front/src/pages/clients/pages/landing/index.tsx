import { Hours } from "@/pages/clients/contents";
import { useGetFoods } from "@/pages/clients/hooks/food";
import {
  Chat,
  FeedBacks,
  Foods,
  Galeries,
  Header,
  HeaderEvent,
  Letter,
  Services,
} from "@/pages/clients/pages/landing/contents";

export const Landing = () => {
  const { data } = useGetFoods();

  return (
    <>
      <Header />
      <Foods data={data || []} />
      <Galeries />
      <Services />
      <HeaderEvent />
      <FeedBacks />
      <Hours />
      <Letter />
      <Chat />
    </>
  );
};
