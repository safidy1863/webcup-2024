import {
  chair1,
  chair2,
  chair3,
  flour,
  flour2,
  food3,
  table,
  textureChocolate,
  textureWhite,
} from "@/assets";
import { Icon } from "@iconify/react";

export const Galeries = () => {
  return (
    <section className="relative bg-chocolate mt-10">
      <img
        src={textureChocolate}
        alt="texture-chocolate"
        className="absolute w-full right-0 left-0 -top-5 tablet:-top-6  laptop:-top-8 desktop:-top-14 large-width:-top-16"
      />
      <div className="padding max-page text-white">
        <div className="flex gap-x-3 items-center justify-end">
          <div className="w-8 h-1 bg-golden -translate-y-1/2"></div>
          <h3 className="z-20">NOS PLUS BELLES VUES</h3>
        </div>
        <h2 className="font-rougth text-6xl text-white text-right mt-2">
          GALERIE
        </h2>

        <div className="grid grid-cols-2 desktop:flex gap-x-3 mt-5">
          <img src={food3} alt="food-3 " className="desktop:w-[600px] h-full large-width:w-[800px] object-cover"/>
          <div className="hidden desktop:grid grid-cols-2 flex-1 grid-rows-2 gap-3">
            <img src={table} alt="table" className="w-full h-full object-cover"/>
            <img src={chair1} alt="chair-1" className="w-full h-full object-cover"/>
            <img src={chair2} alt="chair-2" className="w-full h-full object-cover"/>
            <img src={chair3} alt="chair-3" className="w-full h-full object-cover"/>
          </div>

          <img src={chair1} alt="chair-1" className="desktop:hidden w-full h-full object-cover"/>
        </div>

        <div className="flex justify-between items-end mt-3">
          <img src={flour} alt="flour1" className="h-16" />
          <div className="text-white text-lg flex gap-x-3">
            <Icon icon="ic:round-star" />
            <Icon icon="ic:round-star" />
            <Icon icon="ic:round-star" />
            <Icon icon="ic:round-star" />
            <Icon icon="ic:round-star" />
          </div>
          <img src={flour2} alt="flour2" className="h-16" />
        </div>
      </div>

      <img
        src={textureWhite}
        alt="texture-chocolate"
        className="absolute -bottom-5 w-full right-0 left-0"
      />
    </section>
  );
};
