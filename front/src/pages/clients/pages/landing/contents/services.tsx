import { DServices } from "@/pages/clients/data";
import { Service } from "@/pages/clients/pages/landing/components";

export const Services = () => {
  return (
    <section className="padding max-page relative pb-2">
      <div className="flex gap-x-3 items-center justify-center">
        <div className="w-8 h-1 bg-golden -translate-y-1/2"></div>
        <h3>NOS SERVICES</h3>
        <div className="w-8 h-1 bg-golden -translate-y-1/2"></div>
      </div>

      <h2 className="font-rougth text-6xl text-chocolate text-center mt-2">
      Notre succès réside dans l'âme du Far West !
      </h2>

      <div className="flex mt-8 justify-between gap-x-5">
        {DServices.map((service, index) => (
          <Service key={index} service={service} />
        ))}
      </div>

     
    </section>
  );
};
