import { Button } from "@/components";
import { Food } from "@/pages/clients/components";
import { pathsClients } from "@/pages/clients/constants";
import { TFood } from "@/pages/clients/types";
import { EButtonForm } from "@/types";
import { Icon } from "@iconify/react";
import { Link } from "react-router-dom";

type TFoodProps = {
  data: TFood[];
};

export const Foods = (props: TFoodProps) => {
  const { data } = props;

  return (
    <section className="padding max-page">
      <div className="flex gap-x-3 items-center justify-center">
        <div className="w-8 h-1 bg-golden -translate-y-1/2"></div>
        <h3>PLATS</h3>
        <div className="w-8 h-1 bg-golden -translate-y-1/2"></div>
      </div>

      <h2 className="font-rougth text-6xl text-chocolate text-center mt-2">
        NOS MEILLEURS PLATS
      </h2>
      <div className="text-chocolate text-lg flex justify-center gap-x-3">
        <Icon icon="ic:round-star" />
        <Icon icon="ic:round-star" />
        <Icon icon="ic:round-star" />
        <Icon icon="ic:round-star" />
        <Icon icon="ic:round-star" />
      </div>
      <br />
      <div className="grid grid-cols-2 desktop:flex mt-5 justify-between gap-5">
        {data
          ?.filter(
            (item) =>
              item.category.toLowerCase() === "Personnalisé".toLowerCase()
          )
          .map((food, id) => (
            <Food key={food.id} id={id + 1} food={food} />
          ))}
      </div>

      <Link to={`/${pathsClients.details}`}>
        <Button
          text="Voir plus"
          leftIcon="mdi:reload"
          type={EButtonForm.CIRCLE}
          className="left-1/2 mt-5 text-chocolate"
        />
      </Link>
    </section>
  );
};
