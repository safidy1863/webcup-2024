import { bas } from "@/assets";
import { Button, ControlledInput, Form } from "@/components";
import { EAdornmentPosition } from "@/types";
import { Icon } from "@iconify/react";
import { useForm } from "react-hook-form";

export const Letter = () => {
  const form = useForm();
  const { control } = form;

  return (
    <section className="bg-chocolate rounded-xl bg-opacity-90 w-full max-w-[1000px] flex flex-col gap-y-3 items-center mx-auto mt-10 py-10 ">
      <h2 className="font-rougth text-6xl text-white text-center mt-2">
        ABONNEZ-VOUS à NOTRE <br />
        NEWSLETTER
      </h2>

      <img src={bas} alt="bas" />

      <Form {...form}>
        <div className="relative w-3/5">
          <ControlledInput
            control={control}
            name="email"
            label=""
            className="bg-white rounded-md w-full"
            placeholder="Entrez votre email"
            adornment={
              <Icon icon="fluent:mail-20-regular" className="text-slate-500" />
            }
            adornmentPosition={EAdornmentPosition.START}
          />
          <Button text="S'inscrire" className="absolute right-1.5 top-1 bg-chocolate text-white rounded-md"/>
        </div>
      </Form>
    </section>
  );
};
