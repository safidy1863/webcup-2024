import { banner, hamburger } from "@/assets";
import { Button } from "@/components";
import { useModal } from "@/hooks";
import { idDialog, pathsClients } from "@/pages/clients/constants";
import { EButtonForm } from "@/types";
import { Link } from "react-router-dom";
import { motion } from "framer-motion"

export const Header = () => {
  const { openModal } = useModal({ modalId: idDialog.reservation });

  return (
    <div className="relative h-screen ">
      <img
        src={banner}
        alt="banner-img"
        className="h-full absolute z-10  w-full right-0 left-0 object-cover"
      />
      <div className="absolute padding max-page  overflow-y-hidden inset-0 z-30 flex items-center justify-between padding">
        <motion.div 
        variants={{
          hidden: { opacity: 0, y: 75},
          visible: { opacity: 1, y:0 },
        }}
        initial="hidden"
        animate="visible"
        transition={{ duration: 1, delay: 0.25 }}
        className="text-white">
          <div className="flex gap-x-1 items-center">
            <div className="w-6 h-1 bg-golden -translate-y-1/2 "></div>
            <h2 className="uppercase px-2">Bienvenue chez </h2>
          </div>

          <h1 className="font-rougth text-[100px] leading-[120px]">
            FARWEST RESTAURANT
          </h1>
          <p>Le parfum des grands espaces dans chaque bouchée, la magie des cowboys à chaque service</p>

          <div className="mt-10 flex gap-4">
            <Button
              text="RESERVATION DE TABLE"
              type={EButtonForm.CIRCLE}
              className="bg-golden hover:shadow-lg motion-safe:hover:bg-chocolate"
              onClick={openModal}
            />
            <Link to={pathsClients.details}>
              <Button text="OUVRIR LE MENU"className="border-2 border-white border-opacity-50 rounded-full hover:bg-white hover:text-black before:transition-all before:duration-500 before:ease-in-out "/>
            </Link>
          </div>
        </motion.div>
        <img
          src={hamburger}
          alt="hamburger"
          className="absolute right-3 w-[500px] hidden desktop:block "
        />
      </div>
    </div>
  );
};
