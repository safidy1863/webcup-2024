import { bannerEvents, textureEventBottom, textureEventTop } from "@/assets";

export const HeaderEvent = () => {
  return (
    <header className="relative h-80 ">
      <img
        src={textureEventTop}
        alt="texture-gray-2"
        className="absolute w-full right-0 left-0 -top-2 large-width:-top-5 z-30"
      />
      <img
        src={bannerEvents}
        alt="banner-events"
        className="absolute inset-0 h-full w-full object-cover"
      />
      <div className="absolute overflow-y-hidden inset-0 z-30 flex flex-col items-center justify-center padding">
        <div className="text-white flex flex-col gap-x-3 justify-center items-center">
          <div className="w-6 h-1 bg-golden -translate-y-1/2"></div>
          <h3>EVENEMENTS</h3>
        </div>

        <h2 className="font-rougth text-6xl text-white">LET'S RODEO</h2>

        <div className="flex gap-x-3 mt-3">
          <button className="bg-redTurk font-rougth p-1 text-xl  text-white rounded-md">
            <p className="py-1 px-4 border border-golden rounded-md">
              LUN 06 MAI
            </p>
          </button>

          <button className="bg-redTurk font-rougth p-1 text-xl  text-white rounded-md">
            <p className="py-1 px-4 border border-golden rounded-md">
              JEU 09 MAI
            </p>
          </button>
        </div>

        <div className="mt-4 text-white">
          <p>SAVOIR PLUS</p>
        </div>
      </div>

      <img
        src={textureEventBottom}
        alt="texture-gray-3"
        className="absolute right-0 left-0 w-full bottom-0 z-30"
      />
    </header>
  );
};
