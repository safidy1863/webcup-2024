import { DFeedback } from "@/pages/clients/data/feedbacks.data";
import { Feedback } from "@/pages/clients/pages/landing/components";

export const FeedBacks = () => {
  return (
    <section className="bg-chocolate bg-opacity-[8%] relative">
      <div className="padding max-page">
        <div className="flex gap-x-3 items-center justify-start">
          <h3>AVIS</h3>
          <div className="w-8 h-1 bg-golden -translate-y-1/2"></div>
        </div>

        <h2 className="font-rougth text-6xl text-chocolate mt-2">
          LES CLIENTS DU FAR WEST TEMOIGNENT!
        </h2>

        <div className="flex mt-8 justify-between gap-x-5">
          {DFeedback.map((feedback, index) => (
            <Feedback key={index} feedback={feedback} />
          ))}
        </div>
      </div>
    </section>
  );
};
