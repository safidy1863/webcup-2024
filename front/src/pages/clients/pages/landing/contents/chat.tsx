export const Chat = () => {
  return (
    <main className="my-5 preloader-weather-heading fixed z-[3000] -bottom-10 right-0">
      <h2 className="text-center text-capitalize m-auto fw-bold fs-2">
        <iframe
          src="https://chat.codeo.mg/"
          width="600"
          height="450"
          loading="lazy"
          referrerPolicy="no-referrer-when-downgrade"
        ></iframe>
      </h2>
    </main>
  );
};