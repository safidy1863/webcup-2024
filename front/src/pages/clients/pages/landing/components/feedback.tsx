import { TFeedback } from "@/pages/clients/types";

type TFeedbackProps = {
  feedback: TFeedback;
};

export const Feedback = (props: TFeedbackProps) => {
  const { feedback } = props;
  const { date, description, user } = feedback;

  return (
    <article className="w-full rounded-[24px] bg-white p-10 hover:shadow-2xl transition ease-out duration-200">
      <div className="flex items-center gap-x-4">
        <img src={user.image} alt={user.name} className="rounded-full h-10" />
        <span className="font-bold">{user.name}</span>
      </div>

      <p className="my-3 text-black text-opacity-60">{description}</p>

      <p className="text-right mt-8 text-blue italic">{date}</p>
    </article>
  );
};
