import { tree } from "@/assets";
import { TService } from "@/pages/clients/types";
import { Icon } from "@iconify/react";

type TServiceProps = {
  service: TService;
};

export const Service = (props: TServiceProps) => {
  const { service } = props;
  const { description, icon, title } = service;

  return (
    <article className="bg-white relative text-chocolate shadow-sm w-full p-3 desktop:p-6 rounded-[34px] pb-24 desktop:pb-32 hover:shadow-2xl transition ease-out duration-200">
      <div className=" text-opacity-80 text-5xl bg-chocolate bg-opacity-5 w-24 aspect-square rounded-[32px] flex items-center justify-center">
        <Icon icon={icon} />
      </div>

      <h4 className="text-xl mt-3 font-bold">{title}</h4>
      <p className="py-4">{description}</p>

      <img src={tree} alt="tree" className="absolute bottom-0 right-3 opacity-50"/>
    </article>
  );
};
