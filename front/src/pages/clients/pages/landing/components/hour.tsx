import { sheeiff } from "@/assets";

type THourProps = {
  day: string;
  first: string;
  last: string;
};

export const Hour = (props: THourProps) => {
  const { day, first, last } = props;

  return (
    <div>
      <h5 className="font-rougth text-3xl text-white">{day}</h5>
      <div className="text-white flex justify-between mt-3">
        <div className="flex items-center gap-x-2">
          <img src={sheeiff} alt="sheeiff-1" />
          <span>{first}</span>
        </div>

        <div className="flex items-center gap-x-2">
          <img src={sheeiff} alt="sheeiff-2" />
          <span>{last}</span>
        </div>
      </div>
    </div>
  );
};
