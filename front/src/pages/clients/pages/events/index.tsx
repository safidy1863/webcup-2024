import { useGetEvents } from "@/pages/clients/hooks/food";
import { EventsContent, Header } from "@/pages/clients/pages/events/contents";

export const Events = () => {
  const { data } = useGetEvents();

  return (
    <>
      <Header />
      <EventsContent events={data || []} />
    </>
  );
};
