import { Event } from "@/pages/clients/pages/events/components";
import { TEvent } from "@/pages/clients/types";

type TEventsContentProps = {
  events: TEvent[];
};

export const EventsContent = (props: TEventsContentProps) => {
  const {events} = props;

  return (
    <section className="padding max-page">
      <div className="grid grid-cols-3 desktop:grid-cols-4 gap-5">
        {events?.map((event) => (
          <Event key={event.id} event={event} />
        ))}
      </div>
    </section>
  );
};
