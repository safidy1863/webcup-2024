import { bannerEvents } from "@/assets";
import { pathsClients } from "@/pages/clients/constants";
import { Link } from "react-router-dom";
import { motion } from "framer-motion"

export const Header = () => {
  return (
    <header className="relative h-80 ">
      <img
        src={bannerEvents}
        alt="banner-events"
        className="absolute inset-0 h-full w-full object-cover"
      />
      <motion.div
      variants={{
        hidden: { opacity: 0, y: 75},
        visible: { opacity: 1, y:0 },
      }}
      initial="hidden"
      animate="visible"
      transition={{ duration: 1, delay: 0.25 }}
      className="absolute overflow-y-hidden inset-0 z-30 flex flex-col items-center justify-center padding">
        <div className="text-white flex flex-col gap-x-3 justify-center items-center">
          <div className="w-8 h-1 bg-golden -translate-y-1/2"></div>
          <h3>EVENEMENTS</h3>
        </div>

        <h2 className="font-rougth text-6xl text-white">LET'S RODEO</h2>

        <div className="flex gap-x-3 mt-3">
          <button className="bg-redTurk font-rougth p-1 text-xl  text-white rounded-md">
            <p className="py-1 px-4 border border-golden rounded-md">
              LUN 06 MAI
            </p>
          </button>

          <button className="bg-redTurk font-rougth p-1 text-xl  text-white rounded-md">
            <p className="py-1 px-4 border border-golden rounded-md">
              JEU 09 MAI
            </p>
          </button>
        </div>

        <Link to={`/${pathsClients.events}`} className="mt-10 text-white">
          <p>SAVOIR PLUS</p>
        </Link>
      </motion.div>
    </header>
  );
};
