import { bgEvent } from "@/assets";
import { API_URL } from "@/constants";
import { TEvent } from "@/pages/clients/types";
import dayjs from "dayjs";

type TEventProps = {
  event: TEvent;
};

export const Event = (props: TEventProps) => {
  const { event } = props;
  const { description, endHour, image, startHour, name, date } = event;

  return (
    <div className="w-full relative min-h-[380px] max-w-[320px]">
      <img
        src={bgEvent}
        alt="bg-event"
        className="w-full h-full absolute inset-0 z-10"
      />
      <img
        src={`${API_URL}/${image}`}
        alt="event-1"
        className="mx-auto w-3/4 top-0  left-1/2 -translate-x-1/2 bottom-1/2 absolute  z-20"
      />
      <div
        className="absolute px-5 py-3 top-[45%] left-0 right-0 bg-chocolate rounded-tr-3xl
      rounded-br-3xl z-30 bg-opacity-90 text-white"
      >
        <h1 className="font-rougth text-2xl">{name}</h1>
        <p className="my-3 line-clamp-3">{description}</p>
        <div className="flex gap-x-3 mt-3 uppercase font-">
          <button className="bg-redTurk font-rougth p-1 text-xl  text-white rounded-md">
            <p className="text-white py-1 px-4 rounded-md">
              {dayjs(date).format("YYYY-MM-DD")}
            </p>
          </button>

          <button className="bg-redTurk font-rougth p-1 text-xl  text-white rounded-md">
            <p className="py-1 px-4 rounded-md">
              {dayjs(startHour).format("HH:mm")} -{" "}
              {dayjs(endHour).format("HH:mm")}
            </p>
          </button>
        </div>
      </div>
    </div>
  );
};
