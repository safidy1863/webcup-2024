import { bannerBasket } from "@/assets";
import { motion } from "framer-motion"

export const Header = () => {
  return (
    <header className="relative h-80 ">
      <img
        src={bannerBasket}
        alt="banner-basket"
        className="absolute inset-0 h-full w-full object-cover"
      />
      <motion.div
      variants={{
        hidden: { opacity: 0, y: 75},
        visible: { opacity: 1, y:0 },
      }}
      initial="hidden"
      animate="visible"
      transition={{ duration: 1, delay: 0.25 }}
      className="absolute overflow-y-hidden inset-0 z-30 flex flex-col items-center justify-center padding">
        <h2 className="font-rougth text-6xl text-white">
          PANIER
        </h2>
        <p className="text-white">Commander l'authenticité de l'Ouest Américain</p>
      </motion.div>
    </header>
  );
};
