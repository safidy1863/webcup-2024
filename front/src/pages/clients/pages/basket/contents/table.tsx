import { Button } from "@/components";
import { showBackdropLoader } from "@/helpers";
import { pathsClients } from "@/pages/clients/constants";
import { usePay } from "@/pages/clients/hooks/food";
import { useSaveLocalStorage } from "@/pages/clients/hooks/use-save-food-localstorage";
import { Order } from "@/pages/clients/pages/basket/components";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export const Table = () => {
  const { foods, resetBasket } = useSaveLocalStorage();
  const [total, setTotal] = useState<number>(0);

  useEffect(() => {
    let firstTotal = 0;
    foods.forEach((food) => (firstTotal += food.price));
    setTotal(firstTotal);
  }, [foods]);

  const actionAfterPayement = (url: string) => {
    showBackdropLoader(false);
    resetBasket();
    window.location.href = url;
  };

  const { mutateAsync } = usePay({ action: actionAfterPayement });

  const handleClickPayement = async () => {
    showBackdropLoader(true, "Paiement en cours...");
    await mutateAsync(total);
  };

  return (
    <div className="padding max-page">
      <div className="w-full text-chocolate border-b border-b-chocolate2 pb-3  px-5 flex gap-x-5 items-center">
        <div className="w-10" />

        <div className="w-2/12" />

        <span className="w-3/12">PRODUCT</span>

        <span className="w-1/12">PRICE</span>

        <div className="w-2/12">QUANTITY</div>

        <span className="flex-1">SUBTOTAL</span>
      </div>{" "}
      <div className="border-b border-b-chocolate2 pb-5 flex flex-col gap-y-3">
        {foods?.map((food, index) => (
          <Order key={index} food={food} setTotal={setTotal} />
        ))}
      </div>
      <p className="py-3">Total panier</p>
      <p className="mt-5 text-chocolate flex justify-between border-b border-b-chocolate2 pb-5">
        <span>SUBTOTAL</span>
        <span>{total}</span>
      </p>
      <p className="mt-5 text-chocolate flex justify-between border-b border-b-chocolate2 pb-5">
        <span>TOTAL</span>
        <span>{total}</span>
      </p>
      <div className="mt-10 flex">
        <Button
          text="PAYER"
          className="bg-chocolate text-white rounded"
          onClick={handleClickPayement}
        />
        <Link to={`/${pathsClients.details}`}>
          <Button text="CONTINUER A VOIR LE MENU" className="!bg-transparent" />
        </Link>
      </div>
    </div>
  );
};
