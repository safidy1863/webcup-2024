import { Header, Table } from "@/pages/clients/pages/basket/contents";

export const Basket = () => {
  return (
    <>
      <Header />
      <Table />
    </>
  );
};
