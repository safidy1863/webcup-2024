import { API_URL } from "@/constants";
import { Number } from "@/pages/clients/components";
import { TFood } from "@/pages/clients/types";
import { Icon } from "@iconify/react";
import { Dispatch, useState } from "react";

type TOrderProps = {
  food: TFood;
  setTotal: Dispatch<React.SetStateAction<number>>;
};

export const Order = (props: TOrderProps) => {
  const { food, setTotal } = props;
  const { image, name, price } = food;
  const [number, setNumber] = useState<number>(1);

  const getNumber = (value: number, isPlus?: boolean) => {
    setNumber(value);
    if (isPlus) {
      setTotal((currentValue) => currentValue + value * price);
    } else {
      setTotal((currentValue) => currentValue - value * price);
    }
  };

  return (
    <div className="w-full  px-5 flex gap-x-5 items-center">
      <button className="text-2xl w-10">
        <Icon icon="ic:outline-close" />
      </button>

      <div className="w-2/12 h-28   rounded-md">
        <img
          src={`${API_URL}/${image}`}
          alt={name}
          className="object-cover h-full bg-gray3 rounded-md"
        />
      </div>
      <span className="w-3/12">{name}</span>

      <span className="w-1/12">${price}</span>

      <div className="w-2/12">
        <Number getNumber={getNumber} currentValue={number} />
      </div>

      <span className="flex-1">${price * number}</span>
    </div>
  );
};
