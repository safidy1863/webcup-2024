import { TService } from "@/pages/clients/types";

export const DServices: TService[] = [
  {
    icon: "solar:chef-hat-linear",
    title: "Service traiteur",
    description: "Plongez dans l'aventure culinaire de l'Ouest avec notre service traiteur, alliant les saveurs du Far West à une expérience gastronomique inoubliable",
  },
  {
    icon: "ion:beer-outline",
    title: "Coin Bar-Café",
    description: "Embarquez pour un voyage au Far West à chaque gorgée dans notre bar café/apéro, où l'ambiance rustique rencontre la convivialité légendaire des saloons",
  },
  {
    icon: "mage:delivery-truck",
    title: "Livraison",
    description: "Recevez l'essence du Far West chez vous avec notre service de livraison, où chaque repas apporte une touche de l'Ouest sauvage à votre porte!",
  },
];
