import { user1, user2, user3 } from "@/assets";
import { TFeedback } from "@/pages/clients/types";

export const DFeedback: TFeedback[] = [
  {
    user: {
      image: user1,
      name: "R.Jean",
    },
    description:
      "L'équipe a dépassé mes attentes. Services rapides, professionnels et avec un vrai souci du détail. Fiabilité et professionnalisme sont les maîtres mots chez MyFrontalier. Je suis client depuis des années et toujours satisfait.",
    date: "2 Mai 2024",
  },
  {
    user: {
      image: user2,
      name: "R.Safidy",
    },
    description:
      "Si vous voyagez dans l'Ouest américain, ne manquez pas de faire un détour par le restaurant Farwest. Leur accueil chaleureux fait que je m'y sens vraiment comme chez moi.",
    date: "2 Mai 2024",
  },
  {
    user: {
      image: user3,
      name: "S.Aina",
    },
    description:
      "Lorsque vous explorez les contrées sauvages de l'Ouest américain, ne manquez pas de faire une halte au Farwest Restaurant. Avec leur ambiance typique et leur service amical, associés à un menu diversifié, votre satisfaction culinaire est garantie à chaque visite.",
    date: "2 Mai 2024",
  },
];
