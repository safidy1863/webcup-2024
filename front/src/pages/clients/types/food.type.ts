export type TFood = {
  id: string;
  image: string;
  name: string;
  category: string;
  price: number;
  description: string;
};
