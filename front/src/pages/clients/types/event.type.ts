export type TEvent = {
  id: number;
  name: string;
  description: string;
  date: string;
  startHour: string;
  endHour: string;
  image: string;
  price: number;
};
