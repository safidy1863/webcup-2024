export type TFeedback = {
  user: {
    image: string;
    name: string;
  };
  description: string;
  date: string;
};
