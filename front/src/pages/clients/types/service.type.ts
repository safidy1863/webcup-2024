export type TService = {
  icon: string;
  title: string;
  description: string;
};
