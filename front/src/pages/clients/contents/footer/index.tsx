import { logo, shapeFooter } from "@/assets";
import { pathsClients } from "@/pages/clients/constants";
import { Maps } from "@/pages/clients/contents/footer/maps";
import { Icon } from "@iconify/react";
import { Link } from "react-router-dom";

export const Footer = () => {
  return (
    <footer className=" mt-32 laptop:min-h-[550px] bg-chocolate laptop:bg-transparent relative">
      <img
        src={shapeFooter}
        alt="shape-footer"
        className="absolute hidden laptop:block h-full w-full inset-0"
      />
      <div className="max-page  padding laptop:absolute inset-0">
        <div className="flex flex-col items-center justify-center pb-5  border-b border-dashed border-chocolate2">
          <img src={logo} alt="logo" className="h-20" />
          <div className="flex text-chocolate2 text-xl gap-x-3 mt-3">
            <Icon icon="basil:facebook-solid" />
            <Icon icon="jam:linkedin" />
            <Icon icon="mdi:instagram" />
            <Icon icon="mdi:youtube" />
          </div>
        </div>

        <div className="mt-5 text-white flex flex-col laptop:flex-row justify-between gap-x-3 border-b gap-y-10 pb-10 border-dashed border-chocolate2">
          <div className="w-full">
            <h4 className="font-rougth text-3xl text-white mb-3">A PROPOS</h4>
            <p className="opacity-60 text-sm font-light">Dans notre restaurant, plongez dans l'aventure du Far West où chaque repas est une expédition culinaire à travers les vastes étendues de saveurs authentiques et de convivialité chaleureuse. Bienvenue dans notre oasis de l'Ouest où l'esprit de l'aventure rencontre la satisfaction gastronomique</p>
          </div>

          <div className="w-full">
            <h4 className="font-rougth text-3xl text-white mb-3">
              LIENS IMPORTANTS
            </h4>
            <ul className="flex flex-col gap-y-1">
              <Link to={`/`}>
                <li>Accueil</li>
              </Link>

              <Link to={`/${pathsClients.details}`}>
                <li>Menu</li>
              </Link>

              <Link to={`/${pathsClients.basket}`}>
                <li>Panier</li>
              </Link>

              <Link to={`/${pathsClients.events}`}>
                <li>Evénements</li>
              </Link>
            </ul>
          </div>

          <div className="w-full">
            <h4 className="font-rougth text-3xl text-white mb-3">CONTACT</h4>
            <ul className="flex flex-col gap-y-1">
              <li className="flex items-center gap-x-3">
                <Icon icon="mingcute:phone-line" className="text-chocolate2" />
                <span>+261 34 57 067 89</span>
              </li>
              <li className="flex items-center gap-x-3">
                <Icon icon="ic:outline-email" className="text-chocolate2" />
                <span>farwest@gmail.com</span>
              </li>
              <li className="flex items-center gap-x-3">
                <Icon
                  icon="line-md:my-location-loop"
                  className="text-chocolate2"
                />
                <span>Mississipi, Etats Unis</span>
              </li>
            </ul>
          </div>

          <div className="w-80 laptop:w-full">
            <h4 className="font-rougth text-3xl text-white mb-3">
              LOCALISATION
            </h4>
            <div className="rounded-md h-48 bg-white">
              <Maps />
            </div>
          </div>
        </div>

        <div className="mt-5 flex flex-col laptop:flex-row gap-y-5 justify-between text-chocolate2">
          <span>&copy; Copyright 2024 | FarWest</span>
          <div className="flex gap-x-10">
            <span>Politique de confidentialité</span>
            <span>Termes & Conditions</span>
            <span>Sécurité</span>
          </div>
        </div>
      </div>
    </footer>
  );
};
