import * as yup from "yup";

export const reservationValidation = yup.object({
  name: yup.string().required("Votre nom est obligatoire"),
  email: yup
    .string()
    .email("Veuillez entrer un adresse email valide")
    .required("Votre adresse email est obligatoire"),
  phone: yup.string().required("Votre numéro de téléphone est obligatoire"),
  categoryId: yup.number().required("Le catégorie id est obligatoire"),
  date: yup.string().required("La date de réservation est obligatoire"),
  time: yup.string().required("L'heure de réservation est obligatoire"),
  message: yup.string().required("Le message de réservation est obligatoire"),
});
