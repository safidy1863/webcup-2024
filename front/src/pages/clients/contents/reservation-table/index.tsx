import {
  Button,
  ControlledInput,
  ControlledTextarea,
  Form,
} from "@/components";
import { showBackdropLoader } from "@/helpers";
import { useModal, useValidationResolver } from "@/hooks";
import { idDialog } from "@/pages/clients/constants";
import { reservationValidation } from "@/pages/clients/contents/reservation-table/validation";
import { useReservation } from "@/pages/clients/hooks/food";
import { EAdornmentPosition } from "@/types";
import { Icon } from "@iconify/react";
import { useForm } from "react-hook-form";

export const ReservationTable = () => {
  const { closeModal } = useModal({ modalId: idDialog.reservation });
  const form = useForm({
    mode: "onChange",
    resolver: useValidationResolver(reservationValidation),
  });

  const { control, handleSubmit } = form;

  const afterAction = () => {
    showBackdropLoader(false);
    closeModal();
  };

  const { mutateAsync } = useReservation({ action: afterAction });

  const handleClickSubmit = () => {
    console.log("Testee");
    handleSubmit(async (data) => {
      showBackdropLoader(true, "Réservation en cours...");
      await mutateAsync(
        data as {
          name: string;
          email: string;
          phone: string;
          categoryId: number;
          date: string;
          message: string;
        }
      );
    })();
  };

  return (
    <div className="p-3 px-10">
      <div className="flex items-center my-3 justify-center">
        <div></div>
        <div className="w-8 h-1 bg-golden -translate-y-1/2 mx-auto"></div>
        <button
          onClick={closeModal}
          className="text-chocolate text-xl absolute right-3"
        >
          <Icon icon="material-symbols:close" />
        </button>
      </div>
      <h3 className="font-rougth text-7xl text-chocolate text-center">
        RESERVATION DE TABLE
      </h3>

      <Form {...form}>
        <h3 className="text-chocolate mt-5">Informations</h3>
        <div className="flex gap-x-3">
          <ControlledInput
            control={control}
            name="name"
            placeholder="Nom"
            className="bg-gray3 border-none rounded-md w-full"
          />
          <ControlledInput
            control={control}
            name="email"
            placeholder="Email"
            className="bg-gray3 border-none rounded-md w-full"
          />
          <ControlledInput
            control={control}
            name="phone"
            placeholder="Numéro de téléphone"
            className="bg-gray3 border-none rounded-md w-full"
          />
        </div>

        <h3 className="text-chocolate mt-5">Tables</h3>
        <div className="flex gap-x-3">
          <ControlledInput
            control={control}
            name="categoryId"
            placeholder="1 Personne"
            className="bg-gray3 border-none rounded-md"
            adornment={<Icon icon="mdi:people" className="text-slate-500" />}
            adornmentPosition={EAdornmentPosition.START}
          />
          <ControlledInput
            control={control}
            name="date"
            type="date"
            placeholder="Date"
            className="bg-gray3 border-none rounded-md"
            adornment={<Icon icon="mdi:people" className="text-slate-500" />}
            adornmentPosition={EAdornmentPosition.START}
          />
          <ControlledInput
            control={control}
            name="time"
            type="time"
            placeholder="Heure"
            className="bg-gray3 border-none rounded-md"
            adornment={
              <Icon icon="tabler:clock-hour-4" className="text-slate-500" />
            }
            adornmentPosition={EAdornmentPosition.START}
          />
        </div>
        <ControlledTextarea
          control={control}
          placeholder="Message"
          name="message"
          className="bg-gray3 border-none rounded-md mt-3 placeholder:text-gray"
        />
      </Form>
      <div className="flex justify-end">
        <Button
          text="RESERVER"
          onClick={handleClickSubmit}
          className="bg-chocolate text-white mt-3"
        />
      </div>
    </div>
  );
};
