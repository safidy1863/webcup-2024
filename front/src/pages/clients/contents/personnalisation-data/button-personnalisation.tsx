import React, { Dispatch } from "react";

type Props = {
  title: string;
  choices: string[];
  setValue: Dispatch<React.SetStateAction<string>>;
};

export const ButtonPersonnalisation = (props: Props) => {
  const { choices, title, setValue } = props;

  const setVal = (value: string) => {
    setValue(value);
  };

  return (
    <div>
      <p>{title}</p>
      {choices.map((choice, index) => (
        <button
          key={index}
          onClick={() => setVal(choice)}
          className="border border-chocolate"
        >
          {choice}
        </button>
      ))}
    </div>
  );
};
