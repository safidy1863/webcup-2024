import { food3 } from "@/assets";
import { Button } from "@/components";
import { useModal } from "@/hooks";
import { idDialog } from "@/pages/clients/constants";
import { ButtonPersonnalisation } from "@/pages/clients/contents/personnalisation-data/button-personnalisation";
import { useGenerateImage } from "@/pages/clients/hooks/use-generate-image";
import { Icon } from "@iconify/react";
import { useState } from "react";

export const PersonnalisationData = () => {
  const { closeModal } = useModal({ modalId: idDialog.personnalisationData });
  const [ingredient1, setIngredient1] = useState<string>("");
  const [ingredient2, setIngredient2] = useState<string>("");
  const [ingredient3, setIngredient3] = useState<string>("");
  const [ingredient4, setIngredient4] = useState<string>("");

  const { mutateAsync } = useGenerateImage();

    const generateImage =  async () => {
        await mutateAsync(`generate me an image composed by those ingredients ${ingredient1},${ingredient2},${ingredient3},${ingredient4}`)
    }

  return (
    <div className="bg-white relative p-3 flex gap-x-3">
      <img src={food3} alt="burger" className="h-[450px] w-1/2" />
      <button
        onClick={closeModal}
        className="text-chocolate text-xl absolute right-3"
      >
        <Icon icon="material-symbols:close" />
      </button>
      <div className="flex-1 flex flex-col gap-y-3">
        <h2>BURGER</h2>
        <p>
          <span>$35.00</span>&nbsp; <span>$35.00</span>
        </p>

        <p>
          Créez votre propre expérience culinaire avec notre concept de burger
          personnalisable, où vous choisissez chaque ingrédient pour concocter
          le burger de vos rêves
        </p>

        <div>
          <ButtonPersonnalisation
            title="Viandes"
            choices={["Viandes", "Poulet", "Viandes"]}
            setValue={setIngredient1}
          />

          <ButtonPersonnalisation
            title="Sauces"
            choices={["BBQ Fumé", "Chill", "Epices Authentiques Ouest"]}
            setValue={setIngredient2}
          />

          <ButtonPersonnalisation
            title="Fromage"
            choices={["Cheddar Fort", "Pepper Jack"]}
            setValue={setIngredient3}
          />

          <ButtonPersonnalisation
            title="Légumes"
            choices={["Tomates", "Oignons", "Cornichon", "Laitue"]}
            setValue={setIngredient4}
          />
        </div>

        <div className="flex items-center">
          <Button text="Générer l'image" className="bg-chocolate text-white" onClick={generateImage}/>
        </div>
      </div>
    </div>
  );
};
