import { logo } from "@/assets";
import { clientSignal } from "@/pages/clients/client-signal";
import { Icon } from "@iconify/react";
import { NavigationDrawer } from "./navigation-drawer";
import { Link } from "react-router-dom";
import { useGetFoods, usePay } from "@/pages/clients/hooks/food";
import { BasketItem } from "@/pages/clients/components/basket-item";
import { Button } from "@/components";
import { showBackdropLoader } from "@/helpers";

const Navigation = () => {
  const { data } = useGetFoods();
  const toggleNavigation = () =>
    (clientSignal.value = {
      ...clientSignal.value,
      openNavigation: true,
    });

  const actionAfterPayement = (url: string) => {
    showBackdropLoader(false);
    window.location.href = url;
  };

  const { mutateAsync } = usePay({ action: actionAfterPayement });

  const handleClickPayement = async () => {
    showBackdropLoader(true, "Paiement en cours...");
    await mutateAsync(3000);
  };

  return (
    <div className="text-white flex justify-between items-center max-page padding z-50 absolute left-0 right-0">
      <Link to={`/`}>
        <img src={logo} alt="logo" className="h-24" />
      </Link>
      <div className="flex items-center gap-x-5">
        <div className="dropdown dropdown-end">
          <div tabIndex={0} role="button">
            <button className=" text-3xl cursor-pointer">
              <Icon icon="mage:basket" />
            </button>
          </div>
          <div
            tabIndex={0}
            className="dropdown-content z-[1] p-2 shadow bg-base-100 flex flex-col gap-y-1  rounded min-w-96"
          >
            {data?.slice(2, 4).map((food, index) => (
              <BasketItem key={index} food={food} />
            ))}

            <div className="mt-5 text-chocolate">
              <div className="flex justify-between">
                <span>SUBTOTAL</span>
                <span>$35.00</span>
              </div>
            </div>

            <div className="mt-1 flex gap-x-3">
              <Button
                text="VOIR LE PANIER"
                className="text-white w-full bg-chocolate rounded-md"
              />
              <Button
                text="PAYER"
                onClick={handleClickPayement}
                className="text-white w-full bg-chocolate rounded-md"
              />
            </div>
          </div>
        </div>
        <button onClick={toggleNavigation} className="text-3xl cursor-pointer">
          <Icon icon="jam:menu" />
        </button>
      </div>
    </div>
  );
};

export { Navigation, NavigationDrawer };
