import { logo } from "@/assets";
import { pathsClients } from "@/pages/clients/constants";
import { NavLink } from "react-router-dom";

type TNavigationProps = {
  path: string;
  label: string;
};

const Item = (props: TNavigationProps) => {
  const { path, label } = props;

  return <NavLink to={path} className={({isActive}) => `font-rougth text-7xl  ${isActive ? 'text-golden' : 'text-white'}`}>{label}</NavLink>;
};

export const NavigationDrawer = () => {
  return (
    <div>
      <img src={logo} alt="logo" className="h-36" />

      <div className="flex flex-col gap-y-1 mt-3">
        <Item path={pathsClients.landing} label="ACCUEIL" />
        <Item path={pathsClients.details} label="MENU" />
        <Item path={pathsClients.basket} label="PANIER" />
        <Item path={pathsClients.events} label="EVENEMENTS" />
      </div>
    </div>
  );
};
