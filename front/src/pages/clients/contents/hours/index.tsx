import { bgHour, textureGray2, textureGray3 } from "@/assets";
import { Button } from "@/components";
import { useModal } from "@/hooks";
import { idDialog, pathsClients } from "@/pages/clients/constants";
import { Hour } from "@/pages/clients/pages/landing/components";
import { EButtonForm } from "@/types";
import { Link } from "react-router-dom";

export const Hours = () => {
  const { openModal } = useModal({ modalId: idDialog.reservation });

  return (
    <section className="relative h-[700px] laptop:h-[500px] desktop:h-[400px]">
      <img
        src={textureGray2}
        alt="texture-gray-2"
        className="absolute w-full right-0 left-0 -top-5 z-30"
      />
      <img
        src={bgHour}
        alt="bg-hour"
        className="absolute h-full w-full inset-0 object-cover z-10"
      />
      <div className="max-page padding  absolute flex justify-between inset-0 z-20">
        <div className="relative w-full">
          <div className="flex flex-col mt-20 laptop:mt-0  laptop:h-full laptop:justify-center">
            <div className="flex gap-x-3 items-center justify-start text-white">
              <div className="w-8 h-1 bg-golden -translate-y-1/2"></div>
              <h3>Bon à savoir</h3>
            </div>

            <h2 className="font-rougth text-6xl text-white mt-2">
              Nos heures d'ouvertures
            </h2>

            <div className="mt-10 flex text-white">
              <Button
                text="RESERVATION DE TABLE"
                type={EButtonForm.CIRCLE}
                className="bg-chocolate"
                onClick={openModal}
              />
              <Link to={`/${pathsClients.details}`}>
                <Button text="OUVRIR LE MENU" />
              </Link>
            </div>
          </div>

          <div className="bg-chocolate mt-10 -mx-5 laptop:mx-0 laptop:mt-0 flex flex-col gap-y-10 px-5 desktop:px-20 py-10 h-max laptop:absolute laptop:-bottom-10 right-0">
            <Hour day="DU DIMANCHE AU MARDI" first="09:00" last="22:00" />
            <Hour day="DU VENDREDI AU SAMEDI" first="11:00" last="19:00" />
          </div>
        </div>
      </div>

      <img
        src={textureGray3}
        alt="texture-gray-3"
        className="absolute right-0 left-0 w-full bottom-0 z-30"
      />
    </section>
  );
};
