import { pathsClients } from "@/pages/clients/constants";
import { Basket, Details, Events, Landing } from "@/pages/clients/pages";
import { TRoute } from "@/types";
import { Navigate } from "react-router-dom";

export const ClientsRouter: TRoute[] = [
  {
    path: pathsClients.landing,
    element: <Landing />,
  },
  {
    path: pathsClients.details,
    element: <Details />,
  },
  {
    path: pathsClients.basket,
    element: <Basket />,
  },
  {
    path: pathsClients.events,
    element: <Events />,
  },
  {
    path: "",
    element: <Navigate to={pathsClients.landing} replace />,
  },
];
