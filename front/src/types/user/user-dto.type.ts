export type TUserDto = {
  pseudo: string;
  email: string;
  password: string;
};
