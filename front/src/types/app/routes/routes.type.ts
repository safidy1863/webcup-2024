import { ReactNode } from "react";

export type TRoute = {
  path: string;
  exact?: boolean;
  element?: ReactNode;
  isProtected?: boolean;
  children?: TRoute[];
  noAccessFallBack?: string;
};
