import { signal } from "@preact/signals-react";

export const backdrop = signal({
  open: false,
  message: "En attente de traitement...",
});

export const showBackdropLoader = (
  open: boolean,
  message = "En attente de traitement..."
) => {
  backdrop.value = { open, message };
};
