import { ETOAST } from "@/types";
import { Icon } from "@iconify/react";
import { toast } from "react-toastify";

export const showToast = (
  content: string,
  type: ETOAST,
  title: string,
  color: string
) => {
  toast(
    <div>
      <h3 className={color}>{title}</h3>
      <p>{content}</p>
    </div>,
    {
      type,
      className: "bg-white",
      icon: (
        <Icon
          icon="fluent-mdl2:status-error-full"
          className={`text-2xl ${color}`}
        />
      ),
    }
  );
};
