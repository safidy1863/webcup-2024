import { ProtectedRoute } from "@/routes";
import { TRoute } from "@/types";
import { ReactElement } from "react";
import { Route } from "react-router-dom";

export const RenderRoutes = (routes: TRoute[]) => {
  return (
    <>
      {routes.map((route) => (
        <Route
          key={route.path}
          path={route.path}
          element={
            <ProtectedRoute
              element={route.element as ReactElement}
              noAccessRedirect={route.noAccessFallBack}
              isProtected={route.isProtected}
            />
          }
        >
          {route.children &&
            route?.children.length > 0 &&
            RenderRoutes(route.children)}
        </Route>
      ))}
    </>
  );
};
