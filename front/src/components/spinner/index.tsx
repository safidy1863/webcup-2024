import { cn } from "@/utils";

type TSpinnerProps = {
  className?: string;
};
import { BackdropLoader } from "./backdrop-loader";

const Spinner = ({ className }: TSpinnerProps) => (
  <div>
    <span className={cn("loading loading-spinner loading-lg", className)} />
  </div>
);

export { Spinner, BackdropLoader };
