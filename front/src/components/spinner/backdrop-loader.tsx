import { Spinner } from "@/components/spinner";
import { backdrop } from "@/helpers";

export const BackdropLoader = () => (
  <div>
    {backdrop.value.open ? (
      <div className="fixed inset-0 z-[100] flex items-center justify-center">
        <div className="absolute inset-0 bg-black opacity-70" />
        <div className="flex z-50 items-center gap-4">
          <Spinner className="text-white" />
          <h1 className="text-white text-lg font-medium">
            {backdrop.value.message}
          </h1>
        </div>
      </div>
    ) : (
      <></>
    )}
  </div>
);
