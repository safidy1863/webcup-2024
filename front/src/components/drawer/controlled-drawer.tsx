/* eslint-disable @typescript-eslint/no-explicit-any */
import { Icon } from "@iconify/react";
import { ReactNode, useEffect, useState } from "react";

type TDrawerProps = {
  open: boolean;
  children: ReactNode;
  size?: "MIN" | "MID" | "MAX";
  title: ReactNode | string;
  className?: string;
  onCancel?: (value?: any) => void;
  onConfirm?: (value?: any) => void;
  onClose?: () => void;
};

const getSize = {
  MAX: "w-[80vw] max-w-[95vw]",
  MID: "w-[40rem]",
  MIN: "w-[30rem]",
};

export const ControlledDrawer = ({
  open,
  children,
  size = "MID",
  onCancel,
  className,
  onClose,
}: TDrawerProps) => {
  const [show, isShow] = useState(false);

  useEffect(() => {
    isShow(open);
  }, [open]);

  const close = () => {
    isShow(false);
    setTimeout(() => {
      if (onCancel) onCancel();
      if (onClose) onClose();
    }, 300);
  };
  return (
    <div className="drawer drawer-end  overflow-x-hidden z-[1000]">
      <input type="checkbox" className="drawer-toggle" checked={show} />
      <div className="drawer-side">
        <label
          onClick={close}
          role="presentation"
          aria-label="close sidebar"
          className="drawer-overlay"
        />
        <div
          className={`z-50 ${getSize[size]} h-screen relative overflow-x-hidden bg-white dark:bg-gray flex flex-col`}
        >
          <div
            className={`z-50 ${getSize[size]} h-screen relative overflow-x-hidden bg-white dark:bg-grayFourdly flex flex-col ${className}`}
          >
           
            <div className=" z-20 p-3 flex justify-between items-center">
              <div></div>
              <button onClick={onClose} className="text-white">
                <Icon icon="ic:outline-close" />
              </button>
            </div>
            <div className="px-5 pb-8 flex-1 overflow-y-auto">
              <div> {children}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
