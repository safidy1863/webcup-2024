/* eslint-disable @typescript-eslint/no-explicit-any */
import { Control, UseFormSetValue } from "react-hook-form";

export type TControlledForm = {
  control: Control<any, any>;
  setValue: UseFormSetValue<any>;
  name: string;
  label?: string;
  description?: string;
};
