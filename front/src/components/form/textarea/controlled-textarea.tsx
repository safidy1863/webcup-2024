import { FC } from "react";
import { Textarea, TTextareaProps } from "./textarea";
import {
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormMessage,
  TControlledForm,
} from "@/components";
import { cn } from "@/utils";

type TControlledInput = TTextareaProps & Omit<TControlledForm, "setValue">;

export const ControlledTextarea: FC<TControlledInput> = ({
  control,
  name,
  placeholder,
  description,
  className,
  type = "text",
  ...props
}) => (
  <FormField
    control={control}
    name={name}
    render={({ field }) => (
      <FormItem className={className}>
        <FormControl>
          <Textarea
            {...field}
            {...props}
            className={cn("w-full", className)}
            type={type}
            placeholder={placeholder}
          />
        </FormControl>
        {description && <FormDescription>{description}</FormDescription>}
        <FormMessage />
      </FormItem>
    )}
  />
);
