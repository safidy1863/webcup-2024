import { EAdornmentPosition } from "@/types";
import { cn } from "@/utils";
import { Icon } from "@iconify/react";
import { InputHTMLAttributes, forwardRef, useId } from "react";

export type TTextareaProps = InputHTMLAttributes<HTMLTextAreaElement> & {
  adornment?: React.ReactNode;
  adornmentPosition?: EAdornmentPosition;
  placeholder?: string;
  classNameInput?: string;
  label?: string;
};

export const Textarea = forwardRef<HTMLInputElement, TTextareaProps>(
  (props) => {
    const {
      disabled,
      adornment,
      adornmentPosition,
      placeholder,
      classNameInput,
      content,
      label,
      ...otherProps
    } = props;
    const id = useId();

    return (
      <div>
        <label htmlFor={id}>
          {label?.includes("*") ? (
            <div className="flex">
              <span className=" font-medium break-all">{label.split("*")}</span>
              <span className="text-[#f00] text-xl">*</span>
            </div>
          ) : (
            <span className="font-medium">{label}</span>
          )}
        </label>
        <div
          className={cn(
            "relative flex max-w-full textarea textarea-bordered bg-transparent"
          )}
        >
          {adornmentPosition === EAdornmentPosition.START && (
            <div className="h-full items-center py-3 px-1">{adornment}</div>
          )}
          <textarea
            id={id}
            placeholder={placeholder}
            className={cn(
              "disabled:text-gray mt-3 bg-transparent disabled:bg-transparent border-none disabled:border-none w-full focus:outline-none focus:border-none dark:text-white",
              adornment ? "px-0" : "px-3",
              classNameInput
            )}
            disabled={disabled}
            min={0}
            {...otherProps}
          />
           

          {content && (
            <div className="block py-4 px-2">
              <Icon icon={content} color="red" fontSize={20} />
            </div>
          )}
          {adornmentPosition === "END" && (
            <div className="h-full items-center py-3 px-1">{adornment}</div>
          )}
        </div>
      </div>
    );
  }
);
