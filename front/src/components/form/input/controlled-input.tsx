import { FC } from "react";
import { Input, TInputProps } from "./input";
import {
  FormControl,
  FormDescription,
  FormField,
  FormItem,
  FormMessage,
  TControlledForm,
} from "@/components";
import { cn } from "@/utils";

type TControlledInput = TInputProps & Omit<TControlledForm, "setValue">;

export const ControlledInput: FC<TControlledInput> = ({
  control,
  name,
  placeholder,
  description,
  className,
  type = "text",
  ...props
}) => (
  <FormField
    control={control}
    name={name}
    render={({ field }) => (
      <FormItem className={className}>
        <FormControl>
          <Input
            {...field}
            {...props}
            className={cn("w-full", className)}
            type={type}
            placeholder={placeholder}
          />
        </FormControl>
        {description && <FormDescription>{description}</FormDescription>}
        <FormMessage />
      </FormItem>
    )}
  />
);
