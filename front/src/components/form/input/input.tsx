import { EAdornmentPosition } from "@/types";
import { cn } from "@/utils";
import { Icon } from "@iconify/react";
import { InputHTMLAttributes, forwardRef, useId, useState } from "react";

export type TInputProps = InputHTMLAttributes<HTMLInputElement> & {
  adornment?: React.ReactNode;
  adornmentPosition?: EAdornmentPosition;
  placeholder?: string;
  classNameInput?: string;
  label?: string;
};

export const Input = forwardRef<HTMLInputElement, TInputProps>((props, ref) => {
  const {
    type,
    disabled,
    adornment,
    adornmentPosition,
    placeholder,
    classNameInput,
    content,
    label,
    ...otherProps
  } = props;
  const id = useId();
  const [show, setShow] = useState(false);
  const isPassword = type === "password";
  const getType = () => {
    if (!isPassword) return type;
    return show ? "text" : "password";
  };

  return (
    <div>
      <label htmlFor={id}>
        {label?.includes("*") ? (
          <div className="flex">
            <span className=" font-medium break-all">
              {label.split("*")}
            </span>
            <span className="text-[#f00] text-xl">*</span>
          </div>
        ) : (
          <span className="font-medium">{label}</span>
        )}
      </label>
      <div
        className={cn(
          "relative flex max-w-full input input-bordered bg-transparent"
        )}
      >
        {adornmentPosition === EAdornmentPosition.START && (
          <div className="h-full items-center py-3 px-1">{adornment}</div>
        )}
        <input
          id={id}
          placeholder={placeholder}
          type={getType()}
          className={cn(
            "disabled:text-gray mt-3 bg-transparent disabled:bg-transparent disabled:border-none w-full focus:outline-none focus:border-none dark:text-white",
            adornment ? "px-0" : "px-3",
            classNameInput
          )}
          ref={ref}
          disabled={disabled}
          min={0}
          {...otherProps}
          onFocus={(e) =>
            e.target.addEventListener(
              "wheel",
              (e) => {
                e.preventDefault();
              },
              { passive: false }
            )
          }
        />
        {isPassword && (
          <div
            className="block py-4 px-2 cursor-pointer hover:opacity-50"
            onClick={() => {
              setShow(!show);
            }}
            role="presentation"
          >
            {show ? (
              <Icon icon="mdi-light:eye-off" fontSize={20} />
            ) : (
              <Icon icon="ph:eye-light" fontSize={20} />
            )}
          </div>
        )}
        {content && (
          <div className="block py-4 px-2">
            <Icon icon={content} color="red" fontSize={20} />
          </div>
        )}
        {adornmentPosition === "END" && (
          <div className="h-full items-center py-3 px-1">{adornment}</div>
        )}

      </div>
    </div>
  );
});
