import { FC } from "react";
import { Icon } from "@iconify/react";
import { FormControl, FormDescription, FormField, FormItem, FormMessage, TControlledForm, TInputProps } from "@/components";
import { cn } from "@/utils";

type TControlledInput = TInputProps & Omit<TControlledForm, "setValue">;

const ControlledSearchBar: FC<TControlledInput> = ({
  control,
  name,
  placeholder,
  description,
  className,
}) => (
  <FormField
    control={control}
    name={name}
    render={({ field }) => (
      <FormItem className={className}>
        <FormControl>
          <div className="flex gap-4 items-center ">
            <label className=" input h-10 input-bordered flex items-center gap-2">
              <Icon icon="iconamoon:search-thin" />
              <input
                type="text"
                className={cn("grow", className)}
                placeholder={placeholder}
                {...field}
              />
            </label>
          </div>
        </FormControl>
        {description && <FormDescription>{description}</FormDescription>}
        <FormMessage />
      </FormItem>
    )}
  />
);

export default ControlledSearchBar;
