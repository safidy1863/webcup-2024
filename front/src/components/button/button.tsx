import { EButtonForm } from "@/types";
import { cn } from "@/utils";
import { Icon } from "@iconify/react";
import { CSSProperties } from "react";

type ButtonProps = {
  type?: EButtonForm;
  text: string;
  className?: string;
  style?: CSSProperties;
  onClick?: VoidFunction;
  leftIcon?: string;
};

export const Button = (props: ButtonProps) => {
  const {
    type = EButtonForm.SQUARE,
    text,
    leftIcon,
    className,
    style,
    onClick,
  } = props;

  return (
    <button
      onClick={onClick}
      className={cn(
        `${
          type === EButtonForm.CIRCLE ? "rounded-3xl" : "rounded-sm"
        }  px-5 py-2  shadow-2xl flex items-center gap-x-2`,
        className
      )}
      style={style}
    >
      {leftIcon && <Icon icon={leftIcon} />}
      <span>{text}</span>
    </button>
  );
};
