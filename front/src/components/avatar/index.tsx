import { useState } from "react";
import { EVatarShape } from "@/types";

type AvatarProps = {
  src?: string;
  size?: number;
  name?: string;
  shape?: EVatarShape ;
  textSize?: number;
};

function Avatar(props: AvatarProps) {
  const {
    src,
    name,
    textSize,
    shape = EVatarShape.CIRCLE,
    size = 15,
  } = props;
  const [isLoading, setIsLoading] = useState(true);

  const handleImageLoaded = () => {
    setIsLoading(false);
  };

  const handleImageErrored = () => {
    setIsLoading(false);
  };
  let nameDefined = "";
  if (src === "" || src === undefined || name !== undefined) {
    const words = name?.split(" ");
    const firstLetters = words?.map((word) => word.charAt(0));
    const result = firstLetters?.join("");
    nameDefined = result?.slice(0, 2) ?? "";
  }

  return (
    <div className={`avatar  rounded-full  w-${size} h-${size}`}>
      {src ? (
        <div
          className={`w-${size}  ${
            shape === EVatarShape.CIRCLE ? "rounded-full" : "rounded-sm"
          }`}
        >
          {isLoading && <div className="skeleton w-full h-full rounded-none" />}
          <img
            src={src}
            alt="avatar"
            onLoad={handleImageLoaded}
            onError={handleImageErrored}
            style={{ display: isLoading ? "none" : "block" }}
          />
        </div>
      ) : (
        <div
          className={`w-${size}  ${
            shape === EVatarShape.CIRCLE ? "rounded-full" : "rounded-sm"
          }`}
          style={{ backgroundColor: "#9acef8" }}
        >
          <h1
            className="h-full flex items-center justify-center text-gray font-medium"
            style={{
              fontSize: textSize ? `${textSize}rem` : "1.5rem",
            }}
          >
            {nameDefined}
          </h1>
        </div>
      )}
    </div>
  );
}

export default Avatar;
