import { ReactNode } from "react";

type TDialogProps = {
  modalId: string;
  children: ReactNode;
};

export const Dialog = (props: TDialogProps) => {
  const { modalId, children } = props;

  return (
    <dialog id={modalId} className="modal">
      <div className="modal-box bg-white w-auto !max-w-[70%] p-0 rounded-md">
        {children}
      </div>
      <form method="dialog" className="modal-backdrop opacity-55">
        <button>close</button>
      </form>
    </dialog>
  );
};
