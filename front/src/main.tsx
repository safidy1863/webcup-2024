import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter, Routes } from "react-router-dom";
import { routes } from "@/routes/routes.tsx";
import { ToastProvider } from "@/contexts";
import { RenderRoutes } from "@/helpers";
import { BackdropLoader } from "@/components";
import { QueryClientProvider } from "@tanstack/react-query";
import { queryClient } from "@/utils";
import "./index.css";

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <ToastProvider>
        <BackdropLoader />
        <BrowserRouter>
          <Routes>{RenderRoutes(routes)}</Routes>
        </BrowserRouter>
      </ToastProvider>
    </QueryClientProvider>
  </React.StrictMode>
);
