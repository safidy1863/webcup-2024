import { paths } from "@/constants";
import { Admin, AdminRouter, Clients, ClientsRouter } from "@/pages";
import { TRoute } from "@/types";
import { Navigate } from "react-router-dom";

export const routes: TRoute[] = [
  {
    path: paths.client,
    element: <Clients />,
    children: ClientsRouter,
  },
  {
    path: paths.admin,
    element: <Admin />,
    children: AdminRouter,
  },
  {
    path: "",
    element: <Navigate to={paths.client} replace />,
  },
];
