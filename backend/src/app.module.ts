import { Module } from "@nestjs/common";
import { PrismaModule } from "./prisma/prisma.module";
import { ConfigModule } from "@nestjs/config";
import { FoodModule } from "./food/food.module";
import { ServeStaticModule } from "@nestjs/serve-static";
import { join } from "path";
import { IngredientModule } from './ingredient/ingredient.module';
import { StripeModule } from './stripe/stripe.module';
import { TableModule } from './table/table.module';
import { EventModule } from './event/event.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, ".."),
    }),
    PrismaModule,
    FoodModule,
    IngredientModule,
    StripeModule,
    TableModule,
    EventModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
