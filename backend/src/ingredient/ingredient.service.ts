import { Injectable } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";

@Injectable()
export class IngredientService {
  constructor(private prismaService: PrismaService) {}

  async fetchAll() {
    return this.prismaService.ingredient.findMany();
  }
}
