import { Controller, Get } from "@nestjs/common";
import { IngredientService } from "./ingredient.service";
import { ApiTags } from "@nestjs/swagger";

@ApiTags("ingredient")
@Controller("ingredient")
export class IngredientController {
  constructor(private ingredientService: IngredientService) {}

  @Get()
  async fetchAll() {
    return this.ingredientService.fetchAll();
  }
}
