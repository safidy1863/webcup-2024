import { Body, Controller, Get, Post } from "@nestjs/common";
import { TableService } from "./table.service";
import { BookTableDto } from "./dto/booktable.dto";
import { ApiTags } from "@nestjs/swagger";

@ApiTags("Table")
@Controller("table")
export class TableController {
  constructor(private tableService: TableService) {}

  @Get()
  getTables() {
    return this.tableService.getTables();
  }

  @Post("book")
  bookTable(@Body() data: BookTableDto) {
    return this.tableService.bookTable(data);
  }

  @Post()
  createTable(@Body() data: { name: string; number: number }) {
    return this.tableService.createTable(data);
  }

  @Get("categories")
  getCategories() {
    return this.tableService.getCategories();
  }
}
