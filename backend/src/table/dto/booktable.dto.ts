import { IsEmail, IsNotEmpty, IsPhoneNumber } from "class-validator";

export class BookTableDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsPhoneNumber()
  phone: string;

  @IsNotEmpty()
  categoryId: number;

  @IsNotEmpty()
  date: string;

  @IsNotEmpty()
  time: string;

  message: string;
}
