import { Injectable, NotFoundException } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";
import { BookTableDto } from "./dto/booktable.dto";
import { DateService } from "src/core/types/services/date.service";

@Injectable()
export class TableService {
  constructor(private prismaService: PrismaService) {}

  async getTables() {
    return this.prismaService.table.findMany({ include: { category: true } });
  }

  async getTableById(id: number) {
    return this.prismaService.table.findUnique({
      where: { number: id },
      include: { category: true, OrderTable: true },
    });
  }

  async bookTable(data: BookTableDto) {
    const categories = await this.prismaService.tableCategory.findUnique({
      where: { id: data.categoryId },
      include: { table: true },
    });

    for (const table of categories.table) {
      const isBooked = await this.prismaService.orderTable.findFirst({
        where: {
          tableNumber: table.number,
          date: new Date(data.date),
        },
      });

      if (!isBooked) {
        const client = await this.prismaService.client.create({
          data: {
            name: data.name,
            email: data.email,
            phone: data.phone,
          },
        });

        return this.prismaService.orderTable.create({
          data: {
            date: new Date(data.date),
            hour: DateService.convertStringToTime(data.time),
            clientId: client.id,
            tableNumber: table.number,
            message: data.message,
          },
        });
      }
    }

    throw new NotFoundException("No table available");
  }

  async createTable(data: { name: string; number: number }) {
    let category = await this.prismaService.tableCategory.findFirst({
      where: { name: data.name },
    });

    if (!category) {
      category = await this.prismaService.tableCategory.create({
        data: { name: data.name },
      });
    }

    return this.prismaService.table.create({
      data: {
        number: data.number,
        category: {
          connect: { id: category.id },
        },
      },
    });
  }

  getCategories() {
    return this.prismaService.tableCategory.findMany();
  }
}
