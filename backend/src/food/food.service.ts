import { Injectable } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";
import { CreateFoodDto } from "./dto";

@Injectable()
export class FoodService {
  constructor(private prismaServce: PrismaService) {}

  async fetchAll() {
    return this.prismaServce.food.findMany({
      include: {
        ingredients: true,
      },
    });
  }

  async fetchPerCategory(category: string) {
    return this.prismaServce.food.findMany({
      where: {
        category: category,
      },
      include: {
        ingredients: true,
      },
    });
  }

  async insert(food: CreateFoodDto, image: string) {
    let ingredientId = [];
    const ingredients = food.ingredients.split(",");

    for (const ingredient of ingredients) {
      const ingredientExists = await this.prismaServce.ingredient.findFirst({
        where: {
          name: ingredient,
        },
      });

      if (!ingredientExists) {
        const newIngredient = await this.prismaServce.ingredient.create({
          data: {
            name: ingredient,
          },
        });

        ingredientId.push(newIngredient.id);
      } else {
        ingredientId.push(ingredientExists.id);
      }
    }

    return this.prismaServce.food.create({
      data: {
        name: food.name,
        description: food.description,
        price: Number(food.price),
        image: image,
        category: food.category,
        ingredients: {
          connect: ingredientId.map((id) => ({ id: id })),
        },
      },
      include: {
        ingredients: true,
      },
    });
  }

  async fetchById(id: number) {
    return this.prismaServce.food.findUnique({
      where: {
        id: id,
      },
      include: {
        ingredients: true,
      },
    });
  }
}
