import { Module } from "@nestjs/common";
import { FoodController } from "./food.controller";
import { FoodService } from "./food.service";
import { PayFoodService } from "./pay.food.service";
import { StripeService } from "src/stripe/stripe.service";

@Module({
  controllers: [FoodController],
  providers: [FoodService, PayFoodService],
})
export class FoodModule {}
