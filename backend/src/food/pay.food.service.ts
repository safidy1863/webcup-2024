import { Injectable } from "@nestjs/common";
import { StripeService } from "src/stripe/stripe.service";
import { FoodService } from "./food.service";
import { url } from "inspector";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class PayFoodService {
  constructor(
    private stripeService: StripeService,
    private foodService: FoodService,
    private configService: ConfigService
  ) {}

  async pay(price: number) {
    const stripe = this.stripeService.getStripeInstance();

    const session = await stripe.checkout.sessions.create({
      payment_method_types: ["card"],
      line_items: [
        {
          price_data: {
            currency: "usd",
            product_data: {
              name: "Food",
            },
            unit_amount: price * 100,
          },
          quantity: 1,
        },
      ],
      mode: "payment",
      success_url: this.configService.get("SUCCESS_URL"),
      cancel_url: this.configService.get("CANCEL_URL"),
    });

    return { url: session.url };
  }
}
