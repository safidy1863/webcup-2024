import { IsNotEmpty, IsString } from "class-validator";

export class CreateFoodDto {
  @IsString()
  @IsNotEmpty()
  name: string;

  @IsString()
  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  price: number;

  @IsString()
  category: string;

  @IsString()
  ingredients: string;
}
