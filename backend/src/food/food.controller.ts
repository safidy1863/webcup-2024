import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from "@nestjs/common";
import { FoodService } from "./food.service";
import { ApiTags } from "@nestjs/swagger";
import { CreateFoodDto } from "./dto";
import { FileInterceptor } from "@nestjs/platform-express";
import { diskStorage } from "multer";
import { PayFoodService } from "./pay.food.service";

@ApiTags("food")
@Controller("food")
export class FoodController {
  constructor(
    private foodService: FoodService,
    private foodPaymentService: PayFoodService
  ) {}

  @Get()
  async fetchAll(@Query("category") category?: string) {
    if (category) {
      return await this.foodService.fetchPerCategory(category);
    }

    return await this.foodService.fetchAll();
  }

  @Post()
  @UseInterceptors(
    FileInterceptor("image", {
      storage: diskStorage({
        destination: (req, file, cb) => {
          const fs = require("fs");
          fs.mkdirSync("../public/img/", { recursive: true });
          cb(null, "public/img/");
        },
        filename: (req, file, cb) => {
          cb(null, file.originalname);
        },
      }),
    })
  )
  async insert(
    @Body() food: CreateFoodDto,
    @UploadedFile() image: Express.Multer.File
  ) {
    return this.foodService.insert(food, image.path);
  }

  @Post("pay")
  async pay(@Body() { price }: { price: number }) {
    return this.foodPaymentService.pay(price);
  }
}
