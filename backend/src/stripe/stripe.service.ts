import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class StripeService {
  constructor(private configService: ConfigService) {}

  getStripeInstance() {
    const stripe = require("stripe");
    return new stripe(this.configService.get("STRIPE_SECRET_KEY"));
  }
}
