import { IsNotEmpty } from "class-validator";

export class CreateEventDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  date: Date;

  @IsNotEmpty()
  startHour: string;

  @IsNotEmpty()
  endHour: string;

  @IsNotEmpty()
  price: number;
}
