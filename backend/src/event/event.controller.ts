import {
  Body,
  Controller,
  Get,
  Post,
  UploadedFile,
  UseInterceptors,
} from "@nestjs/common";
import { EventService } from "./event.service";
import { FileInterceptor } from "@nestjs/platform-express";
import { diskStorage } from "multer";
import { CreateEventDto } from "./dto/create.event.dto";

@Controller("event")
export class EventController {
  constructor(private eventService: EventService) {}

  @Get()
  getEvents() {
    return this.eventService.getEvents();
  }

  @Post()
  @UseInterceptors(
    FileInterceptor("image", {
      storage: diskStorage({
        destination: (req, file, cb) => {
          const fs = require("fs");
          fs.mkdirSync("../public/img/", { recursive: true });
          cb(null, "public/img/");
        },
        filename: (req, file, cb) => {
          cb(null, file.originalname);
        },
      }),
    })
  )
  createEvent(
    @Body() data: CreateEventDto,
    @UploadedFile() image: Express.Multer.File
  ) {
    return this.eventService.createEvent(data, image.path);
  }

  @Post("pay")
  async pay(@Body() { eventId, number }: { eventId: number; number: number }) {
    return this.eventService.buyTicket(eventId, number);
  }
}
