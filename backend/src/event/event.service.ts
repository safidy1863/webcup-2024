import { Injectable } from "@nestjs/common";
import { PrismaService } from "src/prisma/prisma.service";
import { CreateEventDto } from "./dto/create.event.dto";
import { DateService } from "src/core/types/services/date.service";
import { StripeService } from "src/stripe/stripe.service";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class EventService {
  constructor(
    private prismaService: PrismaService,
    private stripeService: StripeService,
    private configService: ConfigService
  ) {}

  async getEvents() {
    return this.prismaService.event.findMany();
  }

  async createEvent(data: CreateEventDto, image: string) {
    return this.prismaService.event.create({
      data: {
        name: data.name,
        description: data.description,
        image: image,
        date: new Date(data.date),
        startHour: DateService.convertStringToTime(data.startHour),
        endHour: DateService.convertStringToTime(data.endHour),
        price: Number(data.price),
      },
    });
  }

  async getEventById(id: number) {
    return this.prismaService.event.findUnique({
      where: {
        id: id,
      },
    });
  }

  async buyTicket(eventId: number, number: number) {
    const event = await this.getEventById(eventId);
    const stripe = this.stripeService.getStripeInstance();

    const session = await stripe.checkout.sessions.create({
      payment_method_types: ["card"],
      line_items: [
        {
          price_data: {
            currency: "usd",
            product_data: {
              name: event.name,
            },
            unit_amount: event.price * 100,
          },
          quantity: number,
        },
      ],
      mode: "payment",
      success_url: this.configService.get("SUCCESS_URL"),
      cancel_url: this.configService.get("CANCEL_URL"),
    });

    return { url: session.url };
  }
}
