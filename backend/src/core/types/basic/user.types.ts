import { ApiProperty } from '@nestjs/swagger';

export class User {
  @ApiProperty()
  id: string;

  @ApiProperty()
  pseudonyme: string;

  @ApiProperty()
  email: string;
}
