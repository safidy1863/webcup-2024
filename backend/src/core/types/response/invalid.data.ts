import { ApiProperty } from '@nestjs/swagger';

export class InvalidDataResponse {
  @ApiProperty()
  message: string[];

  @ApiProperty()
  error: string;

  @ApiProperty()
  statusCode: number;
}
