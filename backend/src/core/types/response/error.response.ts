import { ApiProperty } from '@nestjs/swagger';
import { BaseResponse } from './base.response';

export class ErrorResponse extends BaseResponse {
  @ApiProperty()
  error: string;

  @ApiProperty()
  statusCode: number;
}
