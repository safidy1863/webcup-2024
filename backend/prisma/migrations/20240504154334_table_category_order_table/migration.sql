-- AlterTable
ALTER TABLE "Food" ADD COLUMN     "orderTableId" INTEGER;

-- CreateTable
CREATE TABLE "Table" (
    "number" INTEGER NOT NULL,
    "categoryID" INTEGER NOT NULL,

    CONSTRAINT "Table_pkey" PRIMARY KEY ("number")
);

-- CreateTable
CREATE TABLE "TableCategory" (
    "id" INTEGER NOT NULL,
    "name" VARCHAR(125) NOT NULL,

    CONSTRAINT "TableCategory_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Client" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(125) NOT NULL,
    "email" VARCHAR(125) NOT NULL,
    "phone" VARCHAR(125),

    CONSTRAINT "Client_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "OrderTable" (
    "id" SERIAL NOT NULL,
    "tableNumber" INTEGER NOT NULL,
    "date" TIMESTAMP(3) NOT NULL,
    "hour" TIME NOT NULL,
    "clientId" INTEGER NOT NULL,

    CONSTRAINT "OrderTable_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Food" ADD CONSTRAINT "Food_orderTableId_fkey" FOREIGN KEY ("orderTableId") REFERENCES "OrderTable"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Table" ADD CONSTRAINT "Table_categoryID_fkey" FOREIGN KEY ("categoryID") REFERENCES "TableCategory"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrderTable" ADD CONSTRAINT "OrderTable_tableNumber_fkey" FOREIGN KEY ("tableNumber") REFERENCES "Table"("number") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "OrderTable" ADD CONSTRAINT "OrderTable_clientId_fkey" FOREIGN KEY ("clientId") REFERENCES "Client"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
