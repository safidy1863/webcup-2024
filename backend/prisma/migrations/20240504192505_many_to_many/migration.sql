/*
  Warnings:

  - You are about to drop the column `clientId` on the `Event` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Event" DROP CONSTRAINT "Event_clientId_fkey";

-- AlterTable
ALTER TABLE "Event" DROP COLUMN "clientId";

-- CreateTable
CREATE TABLE "_ClientToEvent" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_ClientToEvent_AB_unique" ON "_ClientToEvent"("A", "B");

-- CreateIndex
CREATE INDEX "_ClientToEvent_B_index" ON "_ClientToEvent"("B");

-- AddForeignKey
ALTER TABLE "_ClientToEvent" ADD CONSTRAINT "_ClientToEvent_A_fkey" FOREIGN KEY ("A") REFERENCES "Client"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_ClientToEvent" ADD CONSTRAINT "_ClientToEvent_B_fkey" FOREIGN KEY ("B") REFERENCES "Event"("id") ON DELETE CASCADE ON UPDATE CASCADE;
