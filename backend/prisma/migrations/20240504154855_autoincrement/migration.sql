-- AlterTable
CREATE SEQUENCE tablecategory_id_seq;
ALTER TABLE "TableCategory" ALTER COLUMN "id" SET DEFAULT nextval('tablecategory_id_seq');
ALTER SEQUENCE tablecategory_id_seq OWNED BY "TableCategory"."id";
